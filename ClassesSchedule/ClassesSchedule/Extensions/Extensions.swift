//
//  Extensions.swift
//  Taxik Client
//
//  Created by Ayham on 27/09/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

//MARK: UIDevice extension
extension UIDevice {
    
    var display: DeviceDisplay {
        
        let screenRect = UIScreen.mainScreen().bounds
        let screenWidth = screenRect.size.width;
        let screenHeight = screenRect.size.height;
        
        // iPad
        if ((screenWidth == 768) && (screenHeight == 1024)) ||
            ((screenWidth == 1024) && (screenHeight == 768)) {
                
                return .iPad;
        }
            // iPhone 3.5 inch
        else if ((screenWidth == 320) && (screenHeight == 480)) ||
            ((screenWidth == 480) && (screenHeight == 320)) {
                
                return .iPhone35Inch;
        }
            // iPhone 4 inch
        else if ((screenWidth == 320) && (screenHeight == 568)) ||
            ((screenWidth == 568) && (screenHeight == 320)) {
                
                return .iPhone4Inch;
        }
            // iPhone 4.7 inch
        else if ((screenWidth == 375) && (screenHeight == 667)) ||
            ((screenWidth == 667) && (screenHeight == 375)) {
                
                return .iPhone47Inch;
        }
            // iPhone 5.5 inch
        else if ((screenWidth == 414) && (screenHeight == 736)) ||
            ((screenWidth == 736) && (screenHeight == 414)) {
                
                return .iPhone55Inch;
        }
            // unknown
        else {
            
            return .Unknown;
        }
    }
}

//MARK: Int extension
extension Int {
    
    func toString() -> String {
        return "\(self)"
    }

    func isEven () -> Bool {
        return (self % 2) == 0
    }

    func isOdd () -> Bool {
        return !isEven()
    }
    
    func downTo (limit: Int, function: (Int) -> ()) {
        
        if limit > self {
            
            return
        }
        
        Array(Array(limit...self).reverse()).each(function)
    }

    func clamp (range: Range<Int>) -> Int {
        return clamp(range.startIndex, range.endIndex - 1)
    }

    func clamp (min: Int, _ max: Int) -> Int {
        return Swift.max(min, Swift.min(max, self))
    }
    
    func isIn (range: Range<Int>, strict: Bool = false) -> Bool {
        
        if strict {
            
            return range.startIndex < self && self < range.endIndex - 1
        }
        
        return range.startIndex <= self && self <= range.endIndex - 1
    }

    func isIn (interval: ClosedInterval<Int>) -> Bool {
        return interval.contains(self)
    }

    func isIn (interval: HalfOpenInterval<Int>) -> Bool {
        return interval.contains(self)
    }
    
    func digits () -> [Int] {
        
        var result = [Int]()
        
        for char in String(self).characters {
            
            let string = String(char)
            if let toInt = Int(string) {
                
                result.append(toInt)
            }
        }
        
        return result
    }
    
    func abs () -> Int {
        return Swift.abs(self)
    }

    func gcd (n: Int) -> Int {
        return n == 0 ? self : n.gcd(self % n)
    }
    
    func lcm (n: Int) -> Int {
        return (self * n).abs() / gcd(n)
    }
    
    /**
        Computes the factorial of self
    
        - returns: Factorial
    */
    func factorial () -> Int {
        return self == 0 ? 1 : self * (self - 1).factorial()
    }
    
    /**
        Random integer between min and max (inclusive).
    
        - parameter min: Minimum value to return
        - parameter max: Maximum value to return
        - returns: Random integer
    */
    static func random(min: Int = 0, max: Int) -> Int {
        return Int(arc4random_uniform(UInt32((max - min) + 1))) + min
    }
}

//MARK: Double extension
extension Double {
    
    func toString() -> String {
        
        return "\(self)"
    }
}

//MARK: Float extension
extension Float {
    
    func toString() -> String {
        
        return "\(self)"
    }
}

//MARK: String extension
extension String {
    
    //return a count of character in string
    var length: Int {
        
        return self.characters.count
    }
    
    //return a length to be use in loop `for`
    var loopLength: Int {
        
        let length = self.length
        if length > 0 {
            
            return length - 1
        }
        else{
            return 0
        }
    }
    
    //converting a string to double
    func toDouble() -> Double? {
        
        // split the string into components
        var comps = self.componentsSeparatedByString(".")
        
        if comps.count == 2{
            if comps[0] != "" && comps[1] != "" {
                
                var whole = 0.0
                if let w = Int(comps[0]) {
                    
                    whole = Double(w)
                }
                else {
                    return nil
                }
                
                var fractional = 0.0
                if let f = Int(comps[1]) {
                    
                    let toThePower = Double(comps[1].characters.count)
                    fractional = Double(f) / pow(10.0, toThePower)
                }
                else {
                    return nil
                }
                
                return whole + fractional
            }
            else{
                return nil
            }
        }
        else if comps.count == 1 {
            
            if let whole = Int(comps[0]) {
                return Double(whole)
            }
            else{
                return nil
            }
        }
        else{
            // we have nothing
            return nil
        }
    }
    
    func removeWhiteSpace() -> String {
        
        return self.stringByReplacingOccurrencesOfString(" ", withString: "")
    }
    
    //remove white space from end of string if found
    func removingWhiteSpaceIfNeeded() -> String {
        
        let predicate = NSPredicate(format: "SELF endswith %@", " ")
        let match: Bool = predicate.evaluateWithObject(self)
        
        if match {
            
            return self.substringToIndex(self.length - 1)
        }
        else{
            return self;
        }
    }
    
    //sub string of string to integer index
    func substringToIndex(index:Int) -> String {
        
        if index < self.length && index >= 0{
            
            return self.substringToIndex(self.startIndex.advancedBy(index))
        }
        else{
            return self
        }
    }
  
    //sub string of string from integer index
    func substringFromIndex(index:Int) -> String {
        
        if index < self.length && index >= 0 {
            return self.substringFromIndex(self.startIndex.advancedBy(index))
        }
        else{
            return self
        }
    }
    
    //return a string in given range
    func substringWithRange(range:Range<Int>) -> String {
        
        if range.startIndex >= 0 &&
           range.startIndex < self.length &&
           range.endIndex > range.startIndex &&
           range.endIndex < self.length &&
           range.endIndex > 0 {
                
            let start = self.startIndex.advancedBy(range.startIndex)
            let end = self.startIndex.advancedBy(range.endIndex)
            return self.substringWithRange(start..<end)
        }
        else{
            return self
        }
    }
    
    //replace a string in given range with given string
    func replaceCharactersInRange(range:Range<Int>, withString: String!) -> String {
        
        if range.startIndex >= 0 &&
           range.startIndex < self.length &&
           range.endIndex > range.startIndex &&
           range.endIndex < self.length &&
           range.endIndex > 0 {
                
            let result: NSMutableString = NSMutableString(string: self)
            result.replaceCharactersInRange(NSRange(range), withString: withString)
            return result as String
        }
        else{
            return self
        }
    }
    
    
    func contains(other: String, ignoreCase: Bool = true) -> Bool {
        
        if ignoreCase {
            
            if let _ = rangeOfString(other, options: .CaseInsensitiveSearch) {
                
                return true
            }
            else{
                
                return false
            }
        }
        else{
            
            if let _ = rangeOfString(other) {
                
                return true
            }
            else{
                
                return false
            }
        }
    }
    
    func percentEscapes() -> String {
        
        if let result = self.stringByRemovingPercentEncoding {
            
            return result
        }
        else{
            
            return ""
        }
    }
    
    mutating func appendWithCommaIfNeeded(other: String) {
        
        if length > 0 && other.length > 0 {
            
            self += ", \(other)"
        }
        else if other.length > 0 {
            
            self += other
        }
    }
    
    //get last character in string
    var last: Character? {
        
        if let last = self[self.length] {
            return last
        }
        else{
            return nil
        }
    }
    
    //get character by index "string[index]"
    subscript(index:Int) -> Character? {
        
        if !self.isEmpty {
            
            let length = self.length
            if index < length && index >= 0 {
                
                return self[self.startIndex.advancedBy(index)]
            }
            else if index < 0{
                
                return self[self.startIndex.advancedBy(0)]
            }
            else{
                
                return self[self.startIndex.advancedBy(length - 1)]
            }
        }
        else{
            return nil
        }
    }
    
    //get a charactes by range "string[startIndex..endIndex]"
    subscript(range:Range<Int>) -> String {
        
        if range.startIndex >= 0 &&
           range.startIndex < self.length &&
           range.endIndex > range.startIndex &&
           range.endIndex < self.length &&
           range.endIndex > 0 {
                
            let start = self.startIndex.advancedBy(range.startIndex)
            let end = self.startIndex.advancedBy(range.endIndex)
            return self[start..<end]
        }
        else{
            return self
        }
    }
}

//MARK: Character extension
extension Character {
    
    func toInt() -> Int? {
        return Int(String(self))
    }
    
    func isDigit() -> Bool {
        return self.toInt() != nil
    }
}

//MARK: Array extension
extension Array {
    
    private var indexesInterval: HalfOpenInterval<Int> {
        return HalfOpenInterval<Int>(0, self.count)
    }
    
    mutating func removeElement<T: Equatable>(element: T) -> Bool {
        
        for (index, value ) in self.enumerate() {
            
            if let internalElement = value as? T {
                
                if internalElement == element {
                    
                    removeAtIndex(index)
                    return true
                }
            }
        }
        
        return false
    }
    
    mutating func shuffle() {
        
        for var i = self.count - 1; i >= 1; i-- {
            
            let j = Int.random(max: i)
            swap(&self[i], &self[j])
        }
    }
    
    mutating func pop() -> Element {
        return removeLast()
    }
    
    mutating func push(newElement: Element) {
        return append(newElement)
    }
    
    mutating func shiftLeft() -> Element {
        return removeAtIndex(0)
    }
    
    mutating func shiftRight() -> Element {
        return removeLast()
    }
    
    mutating func unshiftLeft (newElement: Element) {
        insert(newElement, atIndex: 0)
    }
    
    mutating func unshiftRight(newElement: Element){
        append(newElement)
    }
    
    mutating func remove <U: Equatable> (element: U) {
        
        let anotherSelf = self
        
        removeAll(keepCapacity: true)
        
        anotherSelf.each { (index: Int, current: Element) in
            
            if current as! U != element {
                
                self.append(current)
            }
        }
    }
    
    func each (call: (Element) -> ()) {
        
        for item in self {
            
            call(item)
        }
    }
    
    func each (call: (Int, Element) -> ()) {
        
        for (index, item) in self.enumerate() {
            
            call(index, item)
        }
    }
    
    func eachRight (call: (Element) -> ()) {
        reverse().each(call)
    }
    
    func eachRight (call: (Int, Element) -> ()) {
        
        for (index, item) in reverse().enumerate() {
            
            call(count - index - 1, item)
        }
    }
    
    func all (test: (Element) -> Bool) -> Bool {
        
        for item in self {
            
            if !test(item) {
                
                return false
            }
        }
        
        return true
    }
    
    func indexOf <U: Equatable> (item: U) -> Int? {
        
        if item is Element {
            
            return unsafeBitCast(self, [U].self).indexOf(item)
        }
        
        return nil
    }
    
    func indexOf (condition: Element -> Bool) -> Int? {
        
        for (index, element) in self.enumerate() {
            
            if condition(element) {
                
                return index
            }
        }
        
        return nil
    }
    
    func max <U: Comparable> () -> U {
        
        return map {
                return $0 as! U
            }.maxElement()!
    }
    
    func min <U: Comparable> () -> U {
        
        return map {
                return $0 as! U
            }.minElement()!
    }
    
    func maxBy <U: Comparable> (call: (Element) -> (U)) -> Element? {
        
        if let firstValue = self.first {
            
            var maxElement: Element = firstValue
            var maxValue: U = call(firstValue)
            
            for i in 1..<self.count {
                
                let element: Element = self[i]
                let value: U = call(element)
                if value > maxValue {
                    
                    maxElement = element
                    maxValue = value
                }
            }
            
            return maxElement
        } else {
            
            return nil
        }
    }
    
    func minBy <U: Comparable> (call: (Element) -> (U)) -> Element? {
        
        if let firstValue = self.first {
            
            var maxElement: Element = firstValue
            var minValue: U = call(firstValue)
            
            for i in 1..<self.count {
                
                let element: Element = self[i]
                let value: U = call(element)
                
                if value < minValue {
                    
                    maxElement = element
                    minValue = value
                }
            }
            
            return maxElement
        } else {
            
            return nil
        }
    }
    
    func contains <T: Equatable> (items: T...) -> Bool {
        return items.all { self.indexOf($0) >= 0 }
    }
    
    func difference <T: Equatable> (values: [T]...) -> [T] {
        
        var result = [T]()
        
        elements: for e in self {
            
            if let element = e as? T {
                
                for value in values {
                    
                    //  if a value is in both self and one of the values arrays
                    //  jump to the next iteration of the outer loop
                    if value.contains(element) {
                        
                        continue elements
                    }
                }
                
                //  element it's only in self
                result.append(element)
            }
        }
        
        return result
    }
    
    func intersection <U: Equatable> (values: [U]...) -> Array {
        
        var result = self
        var intersection = Array()
        
        for (i, value) in values.enumerate() {
            
            //  the intersection is computed by intersecting a couple per loop:
            //  self n values[0], (self n values[0]) n values[1], ...
            if (i > 0) {
                
                result = intersection
                intersection = Array()
            }
            
            //  find common elements and save them in first set
            //  to intersect in the next loop
            value.each { (item: U) -> Void in
                if result.contains(item) {
                    intersection.append(item as! Element)
                }
            }
        }
        
        return intersection
    }
    
    func union <U: Equatable> (values: [U]...) -> Array {
        
        var result = self
        
        for array in values {
            
            for value in array {
                
                if !result.contains(value) {
                    
                    result.append(value as! Element)
                }
            }
        }
        
        return result
    }
    
    
    
    func get (index: Int) -> Element? {
        
        //  If the index is out of bounds it's assumed relative
        func relativeIndex (index: Int) -> Int {
            
            var _index = (index % count)
            
            if _index < 0 {
                _index = count + _index
            }
            
            return _index
        }
        
        let _index = relativeIndex(index)
        return _index < count ? self[_index] : nil
    }
    
    func get (range: Range<Int>) -> Array {
        return self[rangeAsArray: range]
    }
    
    func any (test: (Element) -> Bool) -> Bool {
        
        for item in self {
            
            if test(item) {
                
                return true
            }
        }
        
        return false
    }
    
    
    func reject (exclude: (Element -> Bool)) -> Array {
        
        return filter {
            
            return !exclude($0)
        }
    }
    
    func take (n: Int) -> Array {
        return self[0..<Swift.max(0, n)]
    }
    
    func takeWhile (condition: (Element) -> Bool) -> Array {
        
        var lastTrue = -1
        
        for (index, value) in self.enumerate() {
            
            if condition(value) {
                
                lastTrue = index
            } else {
                
                break
            }
        }
        
        return take(lastTrue + 1)
    }
    
    func takeFirst (condition: (Element) -> Bool) -> Element? {
        
        for value in self {
            if condition(value) {
                return value
            }
        }
        
        return nil
        
    }
    
    func tail (n: Int) -> Array {
        return self[(count - n)..<count]
    }
    
    func skip (n: Int) -> Array {
        return n > count ? [] : self[n..<count]
    }
    
    func skipWhile (condition: (Element) -> Bool) -> Array {
        
        var lastTrue = -1
        
        for (index, value) in self.enumerate() {
            
            if condition(value) {
                
                lastTrue = index
            } else {
                
                break
            }
        }
        
        return skip(lastTrue + 1)
    }
    
    func unique <T: Equatable> () -> [T] {
        
        var result = [T]()
        
        for item in self {
            
            if !result.contains(item as! T) {
                
                result.append(item as! T)
            }
        }
        
        return result
    }
    
    func uniqueBy <T: Equatable> (call: (Element) -> (T)) -> [Element] {
        
        var result: [Element] = []
        var uniqueItems: [T] = []
        
        for item in self {
            
            let callResult: T = call(item)
            if !uniqueItems.contains(callResult) {
                
                uniqueItems.append(callResult)
                result.append(item)
            }
        }
        
        return result
    }
    
    func countWhere (test: (Element) -> Bool) -> Int {
        
        var result = 0
        
        for item in self {
            
            if test(item) {
                
                result++
            }
        }
        
        return result
    }
    
    func mapFilter <V> (mapFunction map: (Element) -> (V)?) -> [V] {
        
        var mapped = [V]()
        
        each { (value: Element) -> Void in
            
            if let mappedValue = map(value) {
                mapped.append(mappedValue)
            }
        }
        
        return mapped
    }
    
    func mapAccum <U, V> (initial: U, mapFunction map: (U, Element) -> (U, V)) -> (U, [V]) {
        
        var mapped = [V]()
        var acc = initial
        
        each { (value: Element) -> Void in
            
            let (mappedAcc, mappedValue) = map(acc, value)
            acc = mappedAcc
            mapped.append(mappedValue)
        }
        
        return (acc, mapped)
    }
    
    func reduce (combine: (Element, Element) -> Element) -> Element? {
        
        if let firstElement = first {
            return skip(1).reduce(firstElement, combine: combine)
        }
        
        return nil
    }
    
    func reduceRight <U> (initial: U, combine: (U, Element) -> U) -> U {
        return reverse().reduce(initial, combine: combine)
    }
    
    func reduceRight (combine: (Element, Element) -> Element) -> Element? {
        return reverse().reduce(combine)
    }
    
    func at (indexes: Int...) -> Array {
        return indexes.map { self.get($0)! }
    }
    
    func toDictionary <U> (keySelector:(Element) -> U) -> [U: Element] {
        
        var result: [U: Element] = [:]
        for item in self {
            
            result[keySelector(item)] = item
        }
        
        return result
    }
    
    func sortBy (isOrderedBefore: (Element, Element) -> Bool) -> [Element] {
        return sort(isOrderedBefore)
    }
    
    static func range <U: ForwardIndexType> (range: Range<U>) -> [U] {
        return [U](range)
    }
    
    subscript (rangeAsArray rangeAsArray: Range<Int>) -> Array {
        
        //  Fix out of bounds indexes
        let start = Swift.max(0, rangeAsArray.startIndex)
        let end = Swift.min(rangeAsArray.endIndex, count)
        
        if start > end {
            
            return []
        }
        
        return Array(self[Range(start: start, end: end)] as ArraySlice<Element>)
    }
    
    subscript (interval: HalfOpenInterval<Int>) -> Array {
        return self[rangeAsArray: Range(start: interval.start, end: interval.end)]
    }
    
    subscript (interval: ClosedInterval<Int>) -> Array {
        return self[rangeAsArray: Range(start: interval.start, end: interval.end + 1)]
    }
    
    subscript (first: Int, second: Int, rest: Int...) -> Array {
        
        let indexes = [first, second] + rest
        return indexes.map { self[$0] }
    }
}

//MARK: NSIndexPath extension
extension NSIndexPath {
    //return true if a indexpaths is equels
    func isEquelToIndexPath(other: NSIndexPath) -> Bool {
        
        if self.row == other.row && self.section == other.section {
            return true
        }
        else{
            return false
        }
    }
}

//MARK: NSDate extension
extension NSDate {
    
    func toStringinRussinFormat() -> String {
        
        return NSDate.toStringRussinFormat(self)
    }
    
    func toStringRussinFromatWithOutTime() -> String {
        
        return NSDate.toStringRussinFromatWithOutTime(self)
    }
    
    func dateComponents() -> NSDateComponents? {
        
        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let components = gregorian?.components([.Year, .Month, .WeekOfYear, .Hour, .Minute, .Second, .TimeZone], fromDate: self)
        return components
    }
    
    func year() -> Int? {
        return self.dateComponents()?.year
    }
    
    func monthOfYear() -> Int? {
        return self.dateComponents()?.month
    }
    
    func weekOfYear() -> Int? {
        return self.dateComponents()?.weekOfYear
    }
    
    func dayOfWeek() -> Int? {
        
        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        gregorian?.firstWeekday = 2 // Sunday == 1, Saturday == 7
        let day = gregorian?.ordinalityOfUnit(.Weekday, inUnit: .WeekOfYear, forDate: self)
        return day
    }
    
    func hour() -> Int? {
        return self.dateComponents()?.hour
    }
    
    func minute() -> Int? {
        return self.dateComponents()?.minute
    }
    
    func second() -> Int? {
        return self.dateComponents()?.second
    }
    
    class func toStringRussinFormat(date: NSDate) -> String {
        
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm EEE dd MMM"
        return formatter.stringFromDate(date)
    }
    
    class func toStringRussinFromatWithOutTime (date: NSDate) -> String {
        
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter.stringFromDate(date)
    }
    
    class func timeIntervalSince1970() -> NSTimeInterval {
        return NSDate().timeIntervalSince1970
    }
    
    class func isAllowedToCreateOrder(depatureTime: NSTimeInterval) -> Bool {
        return false
    }
    
    class func getDaysDifferenceBetween(dateA:NSDate, and dateB: NSDate) -> Int {
        return 0
    }
}

//MARK: AFNetworkReachabilityStatus extension
extension AFNetworkReachabilityStatus: CustomStringConvertible, CustomDebugStringConvertible {
    
    public var description: String {
        
        switch self {
            
            case .Unknown:
                return "Unknown"
            case .NotReachable:
                return "Not reachable"
            case .ReachableViaWiFi:
                return "Reachable via WiFi"
            case .ReachableViaWWAN:
                return "Reachable via WWAN"
        }
    }
    
    public var debugDescription: String {
        
        return self.description
    }
}

//MARK: JSON extension
extension JSON {
}

//MARK: Operator overloading

/**
    append character to string
*/
func + (string:  String, char: Character) -> String{
    return string + String(char)
}

/**
    Remove an element from the array
*/
func - <T: Equatable> (first: Array<T>, second: T) -> Array<T> {
    return first - [second]
}

/**
    Difference operator
*/
public func - <T: Equatable> (first: Array<T>, second: Array<T>) -> Array<T> {
    return first.difference(second)
}

/**
    Intersection operator
*/
public func & <T: Equatable> (first: Array<T>, second: Array<T>) -> Array<T> {
    return first.intersection(second)
}

/**
    Union operator
*/
public func | <T: Equatable> (first: Array<T>, second: Array<T>) -> Array<T> {
    return first.union(second)
}

public func ~> (lhs: String, rhs: String) -> Bool {
    return lhs.contains(rhs)
}
