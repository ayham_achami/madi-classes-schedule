//
//  RequestManager.swift
//  ClassesSchedule
//
//  Created by Ayham on 05/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import Foundation

//MARK: Cache keys
let GroupsListCacheKey = "GroupsListCacheKey"
let TeachersListCacheKey = "TeachersListCacheKey"
let ClassesCacheKey = "ClassesCacheKey"
let ExamsCacheKey = "ExamsCacheKey"
let TeacherClassesCacheKey = "TeacherClassesCacheKey"

class RequestManager {
    
    //MARK: Static properties
    static var cache: [String: Any] = [:]

    //MARK: Public Class functions
    class func loadGroupsList(compilation: ([Group]?, APIError?) -> Void) {
        
        if let groupFromCache = cache[GroupsListCacheKey] as? [Group]{
            
            compilation(groupFromCache, nil)
        }
        else{
            
            let requestName = MADIAPIReques.GetGroups
            MADIAPIManager.sharedManager.sendRequestWithMethod(.Get, requestName: requestName, requestParameters: nil, success: { (json: JSON) -> Void in
                
                Log.debug(info: json.description);
                if let groups = ParseManager.parseGroupsList(JSON: json) {
                    
                    self.cache[GroupsListCacheKey] = groups
                    compilation(groups, nil)
                }
                else{
                    
                    let message = NSLocalizedString("JSON_deserialize_error", comment: "error")
                    let messageHuman = NSLocalizedString("JSON_deserialize_error_human", comment: "error")
                    let userInfo = [NSLocalizedFailureReasonErrorKey: message,
                                    NSLocalizedDescriptionKey: messageHuman]
                    let domain = MADIAPIManager.sharedManager.BaseURL + requestName.rawValue
                    let error = NSError(domain: domain, code: unableToParseREsponseErrorCode, userInfo: userInfo)
                    let apiError = APIError(error: error, type: ErrorType.Internal)
                    
                    compilation(nil, apiError)
                }
                
                }, failure: { (error: APIError) -> Void in
                    
                    compilation(nil, error)
            })
        }
    }
    
    class func loadClassesScheduleForGroup(group: Group, compilation compilationClosure: (Group, [UserClass]?, APIError?) -> Void) {
        
        if let classesFromCache = cache[ClassesCacheKey + group.name] as? [UserClass] {
            
            compilationClosure(group, classesFromCache, nil)
        }
        else{
            
            let requestName = MADIAPIReques.GetSchedulebyGroup
            let parameters = ["group": ["name": group.name, "id": group.id]]
            MADIAPIManager.sharedManager.sendRequestWithMethod(.Post, requestName: requestName, requestParameters: parameters ,success: { (json: JSON) -> Void in
                
                if let classes = ParseManager.parseUesrClasses(JSON: json) {
            
                    self.cache[ClassesCacheKey + group.name] = classes
                    compilationClosure(group, classes, nil)
                }
                else{
                    
                    let message = NSLocalizedString("JSON_deserialize_error", comment: "error")
                    let messageHuman = NSLocalizedString("JSON_deserialize_error_human", comment: "error")
                    let userInfo = [NSLocalizedFailureReasonErrorKey: message,
                        NSLocalizedDescriptionKey: messageHuman]
                    let domain = MADIAPIManager.sharedManager.BaseURL + requestName.rawValue
                    let error = NSError(domain: domain, code: unableToParseREsponseErrorCode, userInfo: userInfo)
                    let apiError = APIError(error: error, type: ErrorType.Internal)
                    
                    compilationClosure(group, nil, apiError)
                }
                
                }, failure: { (error: APIError) -> Void in
                    
                    compilationClosure(group, nil, error)
            })
        }
    }
    
    class func loadExamsForGroup(group: Group, compilation compilationClosure: (Group, [Exam]?, APIError?) -> Void) {
        
        if let examsFromCache = cache[ExamsCacheKey + group.name] as? [Exam] {
            
            compilationClosure(group, examsFromCache, nil)
        }
        else{
            
            let requestName = MADIAPIReques.GetExamByGroup
            let parameters = ["group": ["name": group.name, "id": group.id]]
            MADIAPIManager.sharedManager.sendRequestWithMethod(.Post, requestName: requestName, requestParameters: parameters ,success: { (json: JSON) -> Void in
                
                if let exams = ParseManager.parseExams(JSON: json) {
                    
                    self.cache[ExamsCacheKey + group.name] = exams
                    compilationClosure(group, exams, nil)
                }
                else{
                    
                    let message = NSLocalizedString("JSON_deserialize_error", comment: "error")
                    let messageHuman = NSLocalizedString("JSON_deserialize_error_human", comment: "error")
                    let userInfo = [NSLocalizedFailureReasonErrorKey: message,
                        NSLocalizedDescriptionKey: messageHuman]
                    let domain = MADIAPIManager.sharedManager.BaseURL + requestName.rawValue
                    let error = NSError(domain: domain, code: unableToParseREsponseErrorCode, userInfo: userInfo)
                    let apiError = APIError(error: error, type: ErrorType.Internal)
                    
                    compilationClosure(group, nil, apiError)
                }
                
                }, failure: { (error: APIError) -> Void in
                    
                    compilationClosure(group, nil, error)
            })
        }
    }
    
    class func loadTeachersList(compilation: ([Teacher]?, APIError?) -> Void) {
        
        if let teachersList = cache[TeachersListCacheKey] as? [Teacher] {
            
            compilation(teachersList, nil)
        }
        else{
            
            let requestName = MADIAPIReques.GetTeachersNames
            MADIAPIManager.sharedManager.sendRequestWithMethod(.Get, requestName: requestName, requestParameters: nil, success: { (json: JSON) -> Void in
                
                if let teachers = ParseManager.parseTeachersList(JSON: json) {
                    
                    self.cache[TeachersListCacheKey] = teachers
                    compilation(teachers, nil)
                }
                else{
                    
                    let message = NSLocalizedString("JSON_deserialize_error", comment: "error")
                    let messageHuman = NSLocalizedString("JSON_deserialize_error_human", comment: "error")
                    let userInfo = [NSLocalizedFailureReasonErrorKey: message,
                        NSLocalizedDescriptionKey: messageHuman]
                    let domain = MADIAPIManager.sharedManager.BaseURL + requestName.rawValue
                    let error = NSError(domain: domain, code: unableToParseREsponseErrorCode, userInfo: userInfo)
                    let apiError = APIError(error: error, type: ErrorType.Internal)
                    
                    compilation(nil, apiError)
                }
                
                }, failure: { (error: APIError) -> Void in
                    
                    compilation(nil, error)
            })
        }
    }
    
    class func loadClassesScheduleForTeacher(teacher: Teacher, compilation compilationClosure: ([WeekDay: [TeacherClass]]?, APIError?) -> Void) {
    
        if let classes = cache[TeacherClassesCacheKey + teacher.name] as? [WeekDay: [TeacherClass]] {
            
            compilationClosure(classes, nil)
        }
        else{
            
            let requestName = MADIAPIReques.GetSchedulebyTeacher
            let parameters: [String: AnyObject] = ["teacher_id": teacher.id , "teacher_name" : teacher.name]
            
            MADIAPIManager.sharedManager.sendRequestWithMethod(.Post, requestName: requestName, requestParameters: parameters ,success: { (json: JSON) -> Void in
                
                if let teacherCalsses = ParseManager.parseTeacherClasses(JSON: json) {
                    
                    let sortedClasses = self.sortClasses(teacherCalsses)
                    self.cache[TeacherClassesCacheKey + teacher.name] = sortedClasses
                    compilationClosure(sortedClasses, nil)
                }
                else{
                    
                    let message = NSLocalizedString("JSON_deserialize_error", comment: "error")
                    let messageHuman = NSLocalizedString("JSON_deserialize_error_human", comment: "error")
                    let userInfo = [NSLocalizedFailureReasonErrorKey: message,
                        NSLocalizedDescriptionKey: messageHuman]
                    let domain = MADIAPIManager.sharedManager.BaseURL + requestName.rawValue
                    let error = NSError(domain: domain, code: unableToParseREsponseErrorCode, userInfo: userInfo)
                    let apiError = APIError(error: error, type: ErrorType.Internal)
                    
                    compilationClosure(nil, apiError)
                }
                
                }, failure: { (error: APIError) -> Void in
                    
                    compilationClosure(nil, error)
            })
        }
    }
    
    class func urlForTeacherPhotoWithId(photoId: String) -> NSURL? {
        
        if photoId != "nil" {
            
            /*
                you can use this URL to test image loading
                NSURL(string: "http://www.madi.ru/uploads/images/11-12-2013/1386753136_1377596540_4503187305.jpg")
            */
            return NSURL(string: "http://www.madi.ru/study/kafedra/images/" + photoId)!
        }
        else{
            
            return nil
        }
    }
    
    //MARK: Private class functions
    private class func sortClasses(classes: [TeacherClass]) -> [WeekDay: [TeacherClass]] {
        
        var result: [WeekDay : [TeacherClass]] = [:]
        
        let mondayClasses = classes.filter{$0.day == .Monday}
        let tuesdayClasses = classes.filter{$0.day == .Tuesday}
        let wednesdayClasses = classes.filter{$0.day == .Wednesday}
        let thursdayClasses = classes.filter{$0.day == .Thursday}
        let fridayClasses = classes.filter{$0.day == .Friday}
        let saturdayClasses = classes.filter{$0.day == .Saturday}
        let saundayClasses = classes.filter{$0.day == .Sunday}
        
        if mondayClasses.count > 0 {
            
            result[.Monday] = mondayClasses
        }
        
        if tuesdayClasses.count > 0 {
            
            result[.Tuesday] = tuesdayClasses
        }
        
        if wednesdayClasses.count > 0 {
            
            result[.Wednesday] = wednesdayClasses
        }
        
        if thursdayClasses.count > 0 {
            
            result[.Thursday] = thursdayClasses
        }
        
        if fridayClasses.count > 0 {
            
            result[.Friday] = fridayClasses
        }
        
        if saturdayClasses.count > 0 {
            
            result[.Saturday] = saturdayClasses
        }
        
        if saundayClasses.count > 0 {
            
            result[.Sunday] = saundayClasses
        }
        
        return result
    }
}
