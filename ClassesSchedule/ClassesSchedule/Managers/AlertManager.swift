//
//  AlertManager.swift
//  ClassesSchedule
//
//  Created by Ayham on 23/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import Foundation

class AlertManager {
    
    //MARK: Public class functions
    class func showAlertOnView(view: UIViewController, title: String, message: String, buttonTitle: String = NSLocalizedString("OK", comment: "OK")) {
    
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let OKAction = UIAlertAction(title: buttonTitle, style: .Default) { (action) in}
        alertController.addAction(OKAction)
        view.presentViewController(alertController, animated: true, completion: nil)
    }
    
    class func showAlertOnView(view: UIViewController, title: String, message: String, buttonTitle: String = NSLocalizedString("OK", comment: "OK"), completion: ()->Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let OKAction = UIAlertAction(title: buttonTitle, style: .Default) { (action) in completion()}
        alertController.addAction(OKAction)
        view.presentViewController(alertController, animated: true, completion: nil)
    }
    
    class func showAlertOnView(view: UIViewController, title: String, message: String, firstButtonTitle: String = NSLocalizedString("Yes", comment: "Yes"), secondButtonTitle: String = NSLocalizedString("No", comment: "No"), firstButtonAction: ()->Void, secondButtonAction: () -> Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: firstButtonTitle, style: .Cancel) { (action) in
            firstButtonAction()
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: secondButtonTitle, style: .Default) { (action) in
            secondButtonAction()
        }
        alertController.addAction(OKAction)
        
        view.presentViewController(alertController, animated: true, completion: nil)
    }
    
    class func showRetryAlertOnView(view: UIViewController, error:APIError, retry: ()->Void) {
        
        let title = NSLocalizedString("Erorr_title", comment: "Erorr_title")
        let format = NSLocalizedString("Retry_alert_message", comment: "Retry_alert_message")
        let message = String.localizedStringWithFormat(format, error.description)
        let firstButtonTitle = NSLocalizedString("Yes", comment: "Yes")
        let secondButtonTitle = NSLocalizedString("No", comment: "No")
        
        showAlertOnView(view, title: title, message: message, firstButtonTitle: firstButtonTitle, secondButtonTitle: secondButtonTitle, firstButtonAction: { () -> Void in
            
                retry()
            
            }, secondButtonAction:{() -> Void in})
    }
    
    class func showRetrySingleButtonAlertOnView(view: UIViewController, error:APIError, retry: ()->Void) {
        
        let title = NSLocalizedString("Erorr_title", comment: "Erorr_title")
        let format = NSLocalizedString("Close_to_retry_alert_message", comment: "Close_to_retry_alert_message")
        let message = String.localizedStringWithFormat(format, error.description)
        let buttonTitle = NSLocalizedString("OK", comment: "OK")
        
        showAlertOnView(view, title: title, message: message, buttonTitle: buttonTitle, completion:{ () -> Void in
            
            retry()
        })
    }
}