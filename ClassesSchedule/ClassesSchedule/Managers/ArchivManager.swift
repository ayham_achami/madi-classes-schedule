//
//  ArchivManager.swift
//  ClassesSchedule
//
//  Created by Ayham on 05/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import Foundation

final class ArchivManager: NSObject, Singletonable {
    
    //MARK: Singleton
    class var sharedManager: ArchivManager {
        
        struct StaticInstance {
            static var instance: ArchivManager? = nil
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticInstance.onceToken) {
            StaticInstance.instance = self.init()
        }
        
        return StaticInstance.instance!
    }
    
    required override init() {}
    
    //MARK: Public properties
    var userGroup: Group? {
        
        get {
            if let data = NSUserDefaults.standardUserDefaults().objectForKey(defaultUserGroupKey) as? NSData{
            
                if let group = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Group {
                
                    return group
                }
                else {
                
                    return nil
                }
            }
            else{
            
                return nil
            }
        }
        set{
            if let newGroup = newValue {
                
                let data = NSKeyedArchiver.archivedDataWithRootObject(newGroup)
                NSUserDefaults.standardUserDefaults().setObject(data, forKey: defaultUserGroupKey)
                NSUserDefaults.standardUserDefaults().synchronize()
            }
        }
    }
    
    class func hasChoiceGroupBefore() -> Bool {
        
        return ArchivManager.sharedManager.userGroup != nil
    }
}