//
//  ParseManager.swift
//  Taxik Client
//
//  Created by Ayham on 18/07/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import UIKit

class ParseManager {
    
    //MARK: Pares data
    class func parseString(json: JSON, objectKey key: String, defaultValue dValue: String = "") -> String{
        
        if let vlaue = json[key].string {
            
            return vlaue
        }
        else{

            Log.debug(info: "Didn't had value for key [\(key)] in JSON {\n\(json.description)\n}")
            return dValue
        }
    }
    
    class func parseInt(json: JSON, objectKey key: String, defaultValue dValue: Int = 0) -> Int {
        
        if let number = json[key].int {
            
            return number
        }
        else{
            
            Log.debug(info: "Didn't had value for key [\(key)] in JSON {\n\(json.debugDescription)\n}")
            return dValue
        }
    }
    
    class func parseUInt(json: JSON, objectKey key: String, defaultValue dValue: UInt = 0) -> UInt {
        
        if let number = json[key].uInt {
            
            return number
        }
        else{
            
            Log.debug(info: "Didn't had value for key [\(key)] in JSON {\n\(json.debugDescription)\n}")
            return dValue
        }
    }
    
    class func parseDouble(json: JSON, objectKey key: String, defaultValue dValue: Double = 0) -> Double {
        
        if let number = json[key].double {
            
            return number
        }
        else{
            
            Log.debug(info: "Didn't had value for key [\(key)] in JSON {\n\(json.debugDescription)\n}")
            return dValue
        }
    }
    
    class func parseFloat(json: JSON, objectKey key: String, defaultValue dValue: Float = 0) -> Float {
        
        if let number = json[key].float {
            
            return number
        }
        else{
            
            Log.debug(info: "Didn't had value for key [\(key)] in JSON {\n\(json.debugDescription)\n}")
            return dValue
        }
    }
    
    class func parseBool(json: JSON, objectKey key: String, defaultValue dValue: Bool = false) -> Bool {
        
        if let number = json[key].int {
            
            if number == 1 {
                
                return true
            }
            else{
                
                return false
            }
        }
        else{
            
            Log.debug(info: "Didn't had value for key [\(key)] in JSON {\n\(json.debugDescription)\n}")
            return dValue
        }
    }
    
    class func parseStringArray(json: JSON, objectKey key: String, defaultValue dValue: [String] = []) -> [String] {
    
        if let array = json[key].array {
            
            var arrayString: [String] = []
            for obj in array {
                
                if let string: String = obj.string {
                    
                    arrayString.append(string)
                }
            }
            
            return arrayString
        }
        else{
            
            return dValue
        }
    }
    
    //MARK: API objects parse
    class func parseServerError(JSON json: JSON, domain aDomain:NSURL) -> NSError {
        
        let code = parseInt(json["error"], objectKey: "code", defaultValue: 0)
        let message = parseString(json["error"], objectKey: "message", defaultValue: "")
        let messageHuman = parseString(json["error"], objectKey: "message_human", defaultValue: "")
        
        let userInfo = [NSLocalizedFailureReasonErrorKey: message,
                        NSLocalizedDescriptionKey: messageHuman]
        let error = NSError(domain: aDomain.absoluteString, code: code, userInfo: userInfo)
        return error
    }
    
    class func parseGroupsList(JSON json: JSON) -> [Group]? {
        
        if let groups = json["groups"].array {
            
            var parsedGroups: [Group] = []
            for value in groups {
                
                let group = Group(JSON: value)
                parsedGroups.append(group)
            }
            
            return parsedGroups
        }
        else{
            
            return nil
        }
    }
    
    class func parseUesrClasses(JSON json: JSON) -> [UserClass]? {
        
        if let schedule = json["schedule"].array {
            
            var parsedClasses: [UserClass] = []
            for value in schedule {
                
                let userClass = UserClass(JSON: value)
                parsedClasses.append(userClass)
            }
            
            return parsedClasses
        }
        else{
            
            return nil
        }
    }
    
    class func parseTeachersList(JSON json: JSON) -> [Teacher]? {
        
        if let teachers = json["teachers"].array {
            
            var parsedTeachers: [Teacher] = []
            for value in teachers {
                
                let teacher = Teacher(JSON: value)
                parsedTeachers.append(teacher)
            }
            
            return parsedTeachers
        }
        else{
            
            return nil
        }
    }
    
    class func parseTeacherClasses(JSON json: JSON) -> [TeacherClass]? {
        
        if let schedule = json["teacher_schedule"].array {
            
            var parsedClasses: [TeacherClass] = []
            for value in schedule {
                
                let teacherClass = TeacherClass(JSON: value)
                parsedClasses.append(teacherClass)
            }
            
            return parsedClasses
        }
        else{
            
            return nil
        }
    }
    
    class func parseExams(JSON json: JSON) -> [Exam]? {
        
        if let exams = json["exams"].array {
            
            var parsedExams: [Exam] = []
            for value in exams {
                
                let exam = Exam(JSON: value)
                parsedExams.append(exam)
            }
            
            return parsedExams
        }
        else{
            
            return nil
        }
    }
}
