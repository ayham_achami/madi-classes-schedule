//
//  CoreDataMeneger.swift
//  ClassesSchedule
//
//  Created by Ayham on 05/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataMeneger: NSObject, Singletonable {
    
    //MARK: Singleton
    class var sharedManager: CoreDataMeneger {
        
        struct StaticInstance {
            static var instance: CoreDataMeneger? = nil
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticInstance.onceToken) {
            StaticInstance.instance = self.init()
        }
        
        return StaticInstance.instance!
    }
    
    required override init() {}
    
    //MARK: Lazy properties
    private lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "madi.test" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Appdb", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Appdb.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            Log.error(error:"Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }
        return coordinator
    }()
    
    private lazy var managedObjectContext: NSManagedObjectContext = {
        
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    //MARK: Private properties
    private var weekType: String {
        
        if let week = NSDate().weekOfYear() {
            
            if week.isEven() {
                
                return "Ч"
            }
            else{
                
                return "З"
            }
        }
        else{
            
            return ""
        }
    }
    
    private var dayOfWeek: Int {
        
        if let day = NSDate().dayOfWeek() {
            
            return day
        }
        else{
            
            return 0
        }
    }
    
    // MARK: Core Data Saving support
    func saveContext () -> Bool {
        
        if self.managedObjectContext.hasChanges {
            
            do {
                
                try managedObjectContext.save()
            } catch {
                
                let nserror = error as NSError
                Log.debug(info:"Unresolved error \(nserror), \(nserror.userInfo)")
                return false;
            }
        }
        
        return true
    }
    
    //MARK: Public functions
    func saveUserClasses(classes: [UserClass]) -> Bool {

        if let groupId = classes.first?.groupId {
            
            deleteSemesterClasses(groupId)
            for classs in classes {
                
                if !insertNewCalss(classs) {
                    
                    return false
                }
            }
            
            return saveContext()
        }
        else{
            
            return false
        }
    }
    
    func getUserClass(group: Group) -> NSFetchedResultsController? {
        
        let request = NSFetchRequest(entityName: "SemesterClass")
        request.sortDescriptors = [NSSortDescriptor(key: "day", ascending: true),
            NSSortDescriptor(key: "startTime", ascending: true, selector: "localizedCaseInsensitiveCompare:")]
        request.predicate = NSPredicate(format: "groupid=%i", group.id)
        
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: "day", cacheName: "Cache")
        
        do {
            
            try frc.performFetch()
            return frc
        } catch let error as NSError {
            
            Log.error(error: error.description)
            return nil
        }
    }
    
    func getWeekCalsses(group: Group) -> NSFetchedResultsController? {
        
        let request = NSFetchRequest(entityName: "SemesterClass")
        request.sortDescriptors = [NSSortDescriptor(key: "day", ascending: true),
            NSSortDescriptor(key: "startTime", ascending: true, selector: "localizedCaseInsensitiveCompare:")]
        request.predicate = NSPredicate(format: "((preiodicity beginswith[c] %@) OR (preiodicity beginswith[c] 'Е')) AND (groupid=%i)",  weekType, group.id)
        
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: "day", cacheName: "Cache")
        
        do {
            
            try frc.performFetch()
            return frc
        } catch let error as NSError {
            
            Log.error(error: error.description)
            return nil
        }
    }
    
    func getTodayCalsses(group: Group) -> NSFetchedResultsController? {
        
        let request = NSFetchRequest(entityName: "SemesterClass")
        request.sortDescriptors = [NSSortDescriptor(key: "day", ascending: true),
            NSSortDescriptor(key: "startTime", ascending: true, selector: "localizedCaseInsensitiveCompare:")]
        request.predicate = NSPredicate(format: "((preiodicity beginswith[c] %@) OR (preiodicity beginswith[c] 'Е')) AND (day= %i) AND (groupid=%i)",  weekType, dayOfWeek ,group.id)
        
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: "day", cacheName: "Cache")
        
        do {
            
            try frc.performFetch()
            return frc
        } catch let error as NSError {
            
            Log.error(error: error.description)
            return nil
        }
    }
    
    func deleteSemesterClasses() {
    
        if let group = ArchivManager.sharedManager.userGroup {
            
            let request = NSFetchRequest(entityName: "SemesterClass")
            request.predicate = NSPredicate(format:"groupid=%i", group.id)
            if let fetchResult = try? managedObjectContext.executeFetchRequest(request) {
                
                for classs in fetchResult {
                    
                    managedObjectContext.deleteObject(classs as! SemesterClass)
                }
                saveContext()
            }
        }
    }

    //MARK: Private methods
    private func insertNewCalss(userClass: UserClass) -> Bool {
        
        let classs = NSEntityDescription.insertNewObjectForEntityForName("SemesterClass", inManagedObjectContext:managedObjectContext) as! SemesterClass
        
        classs.day = NSNumber(integer: userClass.day.rawValue)
        classs.classType = userClass.type
        classs.endTime = userClass.endTime
        classs.groupid = NSNumber(integer: userClass.groupId)
        classs.preiodicity = userClass.periodicity
        classs.room = userClass.roomNumber
        classs.startTime = userClass.startTime
        classs.teacherName = userClass.teacherName
        classs.title = userClass.title
        classs.groupName = userClass.groupName
        classs.techerPhotoId = userClass.teacherPhotoId
        
        return true
    }
    
    private func deleteSemesterClasses(groupId: Int) {
        
        let request = NSFetchRequest(entityName: "SemesterClass")
        request.predicate = NSPredicate(format:"groupid=%i", groupId)
        
        if let fetchResult = try? managedObjectContext.executeFetchRequest(request) {
            
            for classs in fetchResult {
                
                managedObjectContext.deleteObject(classs as! SemesterClass)
            }
            saveContext()
        }
    }
    
    func clearAppData() {
        
        let stores = persistentStoreCoordinator.persistentStores
        for store in stores {
            
            do {
                
                try persistentStoreCoordinator.removePersistentStore(store )
            } catch let error as NSError {
                
                Log.error(error: error.description)
            }
            if let path = store.URL?.path {
                
                do {
                    
                    try NSFileManager.defaultManager().removeItemAtPath(path)
                } catch let error as NSError {
                    
                    Log.error(error: error.description)
                }
            }
        }
    }
}