//
//  TaxikClientAPIManager.swift
//  Taxik Client
//
//  Created by Ayham on 16/10/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

//MARK: Public Closures
typealias SuccessClosure = (JSON) -> Void
typealias FailureClosure = (APIError) -> Void

//MARK: Private Closures
private typealias BaseSuccessClosure = (AnyObject, NSURLRequest) -> Void
private typealias BaseFailureClosure = (NSError, NSURLRequest) -> Void

//MAEK: Default overlay view mode
let overlayViewMode = MRProgressOverlayViewMode.Indeterminate

final class MADIAPIManager: NSObject, Singletonable {
    
    //MARK: Singleton
    class var sharedManager: MADIAPIManager {
        
        struct StaticInstance {
            static var instance: MADIAPIManager? = nil
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&StaticInstance.onceToken) {
            StaticInstance.instance = self.init()
        }
        
        return StaticInstance.instance!
    }
    
    required override init() {}
    
    //MARK: Private properties
    private var _httpSessionManager: AFHTTPSessionManager!
    private var _reachabilityStatus: AFNetworkReachabilityStatus = AFNetworkReachabilityStatus.NotReachable
    private var _isCanceledAllRequests: Bool = false
    
    //MARK: Publci properties
    var showProgressOverlayView: Bool = true
    
    //MARK: Public readonly properties
    var reachabilityStatus: AFNetworkReachabilityStatus {
        return _reachabilityStatus
    }
    
    var BaseURL : String {
        
        let taxikClientAPIProductionServerURL = "http://tplan.madi.ru/madiapp/"
        let taxikClientAPITestServerURL = "http://tplan.madi.ru/madiapp/"
        
        if AppConfiguration.isDebugMode {
            
            return taxikClientAPITestServerURL
        }
        else{
            
            return taxikClientAPIProductionServerURL
        }
    }
    
    //MARK: Configuretion
    func configureManagerWithBaseURL() {
        
        let serverURL = NSURL(string: BaseURL)
        Log.debug(info: "Current API URL: [\(BaseURL)]")
        
        if _httpSessionManager?.operationQueue.operationCount > 0 {
            
            Log.debug(info: "Current operations list which will be canceled: [\(_httpSessionManager?.operationQueue.operations)]")
            _httpSessionManager?.operationQueue.cancelAllOperations()
            _httpSessionManager = nil
        }
        
        let JSONRequestSerializer : AFJSONRequestSerializer = AFJSONRequestSerializer()
        JSONRequestSerializer.setValue("application/json" , forHTTPHeaderField: "Accept")
        JSONRequestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        _httpSessionManager = AFHTTPSessionManager(baseURL: serverURL)
        _httpSessionManager.operationQueue.maxConcurrentOperationCount = 1
        _httpSessionManager.requestSerializer = JSONRequestSerializer
        
        _httpSessionManager.reachabilityManager.startMonitoring()
        _httpSessionManager.reachabilityManager.setReachabilityStatusChangeBlock({(newState: AFNetworkReachabilityStatus) in
            
            if self._reachabilityStatus != newState {
                
                NSNotificationCenter.defaultCenter().postNotificationName(NC_REACHABILITY_STATUS_CHANGED_NOTIFICATION, object: self, userInfo: [UIDK_REACHABILITY_STATUS : newState.rawValue])
                Log.debug(info: "Reachability status was chnaged form [\(self._reachabilityStatus.description)] to [\(newState)]")
                self._reachabilityStatus = newState;
            }
        })
    }
    
    //MARK: HTTP management public methods
    func sendRequestWithMethod(method: HttpRequestMethod,requestName name: MADIAPIReques,                              requestParameters parameters: Dictionary<String, AnyObject>?, success successClosure: SuccessClosure, failure failureClosure: FailureClosure) {
        
        switch method {
            
        case .Get:
            GETRequest(name,
                parameters: parameters,
                success: { (response: AnyObject, URLRequest: NSURLRequest) -> Void in
                    
                    self.checkResponse(response,
                        request: URLRequest,
                        success: { (json: JSON) -> Void in
                            
                            Log.debug(info: "Server response: \(json.description)")
                            successClosure(json)
                        },
                        failure: { (apiError: APIError) -> Void in
                            
                            Log.debug(info: "API Error: \(apiError.description)")
                            failureClosure(apiError)
                    })
                },
                failure: { (error: NSError, URLRequest: NSURLRequest) -> Void in
                    
                    let apiError = APIError(error: error, type: .Network)
                    Log.debug(info: "API Error: \(apiError.description)")
                    failureClosure(apiError)
            })
        case .Post:
            POSTRequest(name,
                parameters: parameters,
                success: { (response: AnyObject, URLRequest: NSURLRequest) -> Void in
                    
                    self.checkResponse(response,
                        request: URLRequest,
                        success: { (json: JSON) -> Void in
                            
                            Log.debug(info: "Server response: \(json.description)")
                            successClosure(json)
                        },
                        failure: { (apiError: APIError) -> Void in
                            
                            Log.debug(info: "API Error: \(apiError.description)")
                            failureClosure(apiError)
                    })
                },
                failure: { (error: NSError, URLRequest: NSURLRequest) -> Void in
                    
                    let apiError = APIError(error: error, type: .Network)
                    Log.debug(info: "API Error: \(apiError.description)")
                    failureClosure(apiError)
            })
        }
    }
    
    func cancelAllRequestsIfNeeded() {
        
        if _httpSessionManager?.operationQueue.operationCount > 0 {
            
            _httpSessionManager?.operationQueue.cancelAllOperations()
            _isCanceledAllRequests = true
            Log.debug(info: "Canceled all network requestes")
        }
        else{
            
            Log.debug(info: "The http client already do't has any active network requestes")
        }
    }
    
    //MARK: HTTP management private functions
    private func GETRequest(requestName: MADIAPIReques, parameters requestParameters: Dictionary<String, AnyObject>?, success successClosure: BaseSuccessClosure, failure failureClosure: BaseFailureClosure) -> AnyObject {
        
        let overlayView = progressOverlayView()
        let task: NSURLSessionTask = _httpSessionManager.GET(requestName.rawValue, parameters: requestParameters, success: {(task: NSURLSessionDataTask!, responseObject: AnyObject!) in
            
            Log.debug(info: "Request [\(task.currentRequest?.URL?.absoluteString)\nSend bytes[\(task.countOfBytesExpectedToSend)]\nBytes expected to send [\(task.countOfBytesExpectedToSend)]\nTask description[\(task.description)]")
            
            successClosure(responseObject, task.originalRequest!)
            },
            failure: {(task: NSURLSessionDataTask!, error: NSError!) in
                
                Log.debug(info: "Request [\(task.currentRequest?.URL?.absoluteString)\nSend bytes[\(task.countOfBytesExpectedToSend)]\nBytes expected to send [\(task.countOfBytesExpectedToSend)]\nTask description[\(task.description)]")
                
                failureClosure(error, task.originalRequest!)
        })!
    
        overlayView?.setModeAndProgressWithStateOfTask(task)
        return task
    }
    
    private func POSTRequest(requestName: MADIAPIReques, parameters requestParameters: Dictionary<String, AnyObject>?, success successClosure: BaseSuccessClosure, failure failureClosure: BaseFailureClosure) -> AnyObject {
        
        let overlayView = progressOverlayView()
        let task: NSURLSessionTask = _httpSessionManager.POST(requestName.rawValue, parameters: requestParameters,
            success: {(task: NSURLSessionDataTask!, responseObject: AnyObject!) in
                
                Log.debug(info: "POSTRequest [\(task.currentRequest?.URL?.absoluteString)\nBytes expected to send [\(task.countOfBytesExpectedToSend)]\nTask description[\(task.description)]")
                successClosure(responseObject, task.originalRequest!)
            },
            failure: {(task: NSURLSessionDataTask!, error: NSError!) in
                
                Log.debug(info: "Request [\(task.currentRequest?.URL?.absoluteString)\nBytes expected to send [\(task.countOfBytesExpectedToSend)]\nTask description[\(task.description)]")
                failureClosure(error, task.originalRequest!)
        })!
        
        overlayView?.setModeAndProgressWithStateOfTask(task)
        return task
    }
    
    //MARK: Private functions
    private func checkResponse(response: AnyObject, request urlrequest: NSURLRequest, success successClosure: SuccessClosure,failure failureClosure: FailureClosure) {
        
        let json = JSON(response)
        
        if json != nil {
            
            if let _ = json["error"].dictionary {
                
                // we have a server error so parse it and invoc failure block
                let apiError = APIError(JSON: json, domin: urlrequest.URL!)
                Log.debug(info: apiError.description)
                failureClosure(apiError)
            }
            else{
                // the respons is fine so invoc success block
                successClosure(json)
            }
        }
        else{
            
            // we have a wrong response or the response is empty !!
            let domain = urlrequest.URL!.absoluteString
            let code = responseIsUndicationarObjectErorrCode
            let message = NSLocalizedString("Response_undicationar", comment: "")
            let errorReason = "Error in server response! Try again later."
            let userInfo = [NSLocalizedFailureReasonErrorKey : errorReason, NSLocalizedDescriptionKey : message]
            
            let error = NSError(domain: domain, code: code, userInfo: userInfo)
            let apiError = APIError(error: error, type: .Server)
            
            Log.debug(info: apiError.description)
            failureClosure(apiError)
        }
    }
    
    private func progressOverlayView() -> MRProgressOverlayView? {
        
        if showProgressOverlayView {
            
            let color = UIApplication.sharedApplication().keyWindow?.rootViewController?.view.tintColor
            let overlayView = MRProgressOverlayView.showOverlayAddedTo(UIApplication.sharedApplication().keyWindow, animated: true)
            overlayView.tintColor = color
            overlayView.titleLabel?.textColor = colorWithRGB(131, green: 147, blue: 161, alpha: 1)
            overlayView.titleLabelText = NSLocalizedString("Loading", comment: "Loading")
            overlayView.mode = overlayViewMode
            
            return overlayView
        }
        else{
            
            return nil
        }
    }
}