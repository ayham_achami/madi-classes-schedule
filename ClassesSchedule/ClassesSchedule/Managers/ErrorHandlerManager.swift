//
//  ErrorHandlerManager.swift
//  ClassesSchedule
//
//  Created by Ayham on 30/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class ErrorHandlerManager {
    
    //MARK: Public class functions
    class func handleError(error: APIError, autoRatry: Bool, completion: (()->Void)?) -> String {

        if autoRatry {
            
            if let view = UIApplication.sharedApplication().keyWindow?.rootViewController {
                
                AlertManager.showRetrySingleButtonAlertOnView(view, error: error, retry: {completion?()})
            }
            else{
                
                Log.error(error: "Then current view controller is nil")
            }
        }
        else{
            
            if error.type == .Server {
                
                if let view = UIApplication.sharedApplication().keyWindow?.rootViewController {
                    
                    let title = ""
                    let message = error.description
                    AlertManager.showAlertOnView(view, title: title, message: message)
                }
                else{
                    
                    Log.error(error: "Then current view controller is nil")
                }
            }
            else{
             
                if let view = UIApplication.sharedApplication().keyWindow?.rootViewController {
                    
                    AlertManager.showRetryAlertOnView(view, error: error, retry: {completion?()})
                }
                else{
                    
                    Log.error(error: "Then current view controller is nil")
                }
            }
        }
        
        return error.description
    }
}