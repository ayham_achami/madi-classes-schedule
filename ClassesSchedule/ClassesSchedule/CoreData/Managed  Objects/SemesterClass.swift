//
//  SemesterClass.swift
//  ClassesSchedule
//
//  Created by Ayham on 09/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import Foundation
import CoreData

@objc(SemesterClass)
class SemesterClass: NSManagedObject {

    @NSManaged var classType: String
    @NSManaged var day: NSNumber
    @NSManaged var endTime: String
    @NSManaged var groupid: NSNumber
    @NSManaged var preiodicity: String
    @NSManaged var room: String
    @NSManaged var startTime: String
    @NSManaged var teacherName: String
    @NSManaged var title: String
    @NSManaged var groupName: String
    @NSManaged var techerPhotoId: String
    
    var classTimeInterval: String {
        
        return startTime + " - " + endTime
    }
}
