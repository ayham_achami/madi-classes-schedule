//
//  Group.swift
//  ClassesSchedule
//
//  Created by Ayham on 05/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class Group: NSObject, NSCoding {
    
    //MARK: Public readonly properties
    var name: String {
        return _name
    }
    
    var id: Int {
        return _id
    }
    
    //MARK: Printable
    override var description: String {
        return "name: \(_name) id:\(_id)"
    }
    
    override var debugDescription: String {
        return description
    }
    
    //MARK: Private properties
    private var _name: String = ""
    private var _id: Int = 0
   
    //Inits
    init(JSON json: JSON){
        
        _name = ParseManager.parseString(json, objectKey: "name")
        _id = ParseManager.parseInt(json, objectKey: "id")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        _name = aDecoder.decodeObjectForKey("name") as! String
        _id = aDecoder.decodeIntegerForKey("id")
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeObject(_name, forKey: "name")
        aCoder.encodeInteger(_id, forKey: "id");
    }
}
