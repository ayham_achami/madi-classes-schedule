//
//  Teacher.swift
//  ClassesSchedule
//
//  Created by Ayham on 11/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class Teacher: NSObject, NSCoding {
 
    //MARK: Public readonly properties
    var id: Int {
        return _id
    }
    
    var name: String {
        return _name
    }
    
    var photo: String {
        return _photo
    }
    
    //MARK: Printable
    override var description: String {
        return "id: \(_id) name: \(_name) photo: \(_photo)"
    }
    
    override var debugDescription: String {
        return description
    }
    
    //MARK: Private properties
    private var _id: Int = 0
    private var _name: String = ""
    private var _photo: String = ""
    
    //MARK: Inits
    init(JSON json: JSON){

        _id = ParseManager.parseInt(json, objectKey: "id")
        _name = ParseManager.parseString(json, objectKey: "name")
        _photo = ParseManager.parseString(json, objectKey: "photo")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        _id = aDecoder.decodeIntegerForKey("id")
        _name = aDecoder.decodeObjectForKey("name") as! String
        _photo = aDecoder.decodeObjectForKey("photo")as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeInteger(_id, forKey: "id");
        aCoder.encodeObject(_name, forKey: "name")
        aCoder.encodeObject(_photo, forKey: "photo")
    }
}
