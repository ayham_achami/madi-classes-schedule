//
//  TeacherClass.swift
//  ClassesSchedule
//
//  Created by Ayham on 11/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class TeacherClass: NSObject, NSCoding {
   
    //MARK: Public readonly properties
    var title: String {
        return _title
    }
    
    var day: WeekDay {
        return _day
    }
    
    var teacherName: String {
        return _teacherName
    }
    
    var type: String {
        return _type
    }
    
    var periodicity: String {
        return _periodicity
    }
    
    var startTime: String {
        return _startTime
    }
    
    var endTime: String {
        return _endTime
    }
    
    var roomNumber: String {
        return _roomNumber
    }
    
    var groupName: String {
        return _groupName
    }
    
    var teacherId: Int {
        return _teacherId
    }
    
    var classTimeInterval: String {
        
        return _startTime + " - " + _endTime
    }
    
    //MARK: Printable
    override var description: String {
        return "room number: \(_roomNumber) day: \(_day) start time: \(_startTime) end time: \(_endTime) group name: \(_groupName) title: \(_title) type: \(_type) periodicity: \(_periodicity) teacher name: \(_teacherName) teacher id: \(_teacherId)"
    }
    
    override var debugDescription: String {
        return description
    }
    
    //MARK: Private properties
    private var _roomNumber: String = ""
    private var _day: WeekDay = .None
    private var _startTime: String = ""
    private var _endTime: String = ""
    private var _groupName: String = ""
    private var _title: String = ""
    private var _type: String = ""
    private var _periodicity: String = ""
    private var _teacherName: String = ""
    private var _teacherId: Int = 0
    
    //MARK: Inits
    init(JSON json: JSON){
        
        _title = ParseManager.parseString(json, objectKey: "class_title")
        _day = WeekDay(rawValue: ParseManager.parseInt(json, objectKey: "day"))
        _teacherName = ParseManager.parseString(json, objectKey: "teacher_name")
        _type = ParseManager.parseString(json, objectKey: "class_type")
        _periodicity = ParseManager.parseString(json, objectKey: "class_periodicity")
        _startTime = ParseManager.parseString(json, objectKey: "start_time")
        _endTime = ParseManager.parseString(json, objectKey: "end_time")
        _roomNumber = ParseManager.parseString(json, objectKey: "room_number")
        _groupName = ParseManager.parseString(json, objectKey: "group_name")
        _teacherId = ParseManager.parseInt(json, objectKey: "teacher_id")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        aDecoder.encodeObject(_title, forKey: "title")
        aDecoder.encodeInteger(_day.rawValue, forKey: "day")
        aDecoder.encodeObject(_teacherName, forKey: "teacherName")
        aDecoder.encodeObject(_type, forKey: "type")
        aDecoder.encodeObject(_periodicity, forKey: "periodicity")
        aDecoder.encodeObject(_startTime, forKey: "startTime")
        aDecoder.encodeObject(_endTime, forKey: "endTime")
        aDecoder.encodeObject(_roomNumber, forKey: "roomNumber")
        aDecoder.encodeObject(_groupName, forKey: "groupName")
        aDecoder.encodeInteger(_teacherId, forKey: "teacherPhotoId")
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        _title = aCoder.decodeObjectForKey("title") as! String
        _day = WeekDay(rawValue: aCoder.decodeIntegerForKey("day"))
        _teacherName = aCoder.decodeObjectForKey("teacherName") as! String
        _type = aCoder.decodeObjectForKey("type") as! String
        _periodicity = aCoder.decodeObjectForKey("periodicity")as! String
        _startTime = aCoder.decodeObjectForKey("startTime") as! String
        _endTime = aCoder.decodeObjectForKey("endTime") as! String
        _roomNumber = aCoder.decodeObjectForKey("roomNumber") as! String
        _groupName = aCoder.decodeObjectForKey("groupName") as! String
        _teacherId = aCoder.decodeIntegerForKey("teacherPhotoId")
    }
}
