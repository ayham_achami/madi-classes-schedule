//
//  UserClass.swift
//  ClassesSchedule
//
//  Created by Ayham on 07/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class UserClass: NSObject, NSCoding {

    //MARK: Public readonly properties
    var title: String {
        return _title
    }
    
    var day: WeekDay {
        return _day
    }
    
    var teacherName: String {
        return _teacherName
    }
    
    var type: String {
        return _type
    }
    
    var periodicity: String {
        return _periodicity
    }
    
    var startTime: String {
        return _startTime
    }
    
    var endTime: String {
        return _endTime
    }
    
    var roomNumber: String {
        return _roomNumber
    }
    
    var groupName: String {
        return _groupName
    }
    
    var groupId: Int {
        return _groupId
    }
    
    var teacherPhotoId: String {
        return _teacherPhotoId
    }
    
    var classTimeInterval: String {
        
        return _startTime + " - " + _endTime
    }
    
    //MARK: Printable
    override var description: String {
        return "title: \(_title) day: \(_day) teacher name: \(_teacherName) periodicity: \(_periodicity) start time: \(_startTime) end time\(_endTime) room number: \(_roomNumber) group name: \(_groupName) group id: \(_groupId) teacher photo id:\(_teacherPhotoId)"
    }
    
    override var debugDescription: String {
        return description
    }
    
    //MARK: Private properties
    private var _title: String = ""
    private var _day: WeekDay = .None
    private var _teacherName: String = ""
    private var _type: String = ""
    private var _periodicity: String = ""
    private var _startTime: String = ""
    private var _endTime: String = ""
    private var _roomNumber: String = ""
    private var _groupName: String = ""
    private var _groupId: Int = 0
    private var _teacherPhotoId: String = ""
    
    //MARK: Inits
    init(JSON json: JSON){
        
        _title = ParseManager.parseString(json, objectKey: "class_title")
        _day = WeekDay(rawValue: ParseManager.parseInt(json, objectKey: "day"))
        _teacherName = ParseManager.parseString(json, objectKey: "teacher_name")
        _type = ParseManager.parseString(json, objectKey: "class_type")
        _periodicity = ParseManager.parseString(json, objectKey: "class_periodicity")
        _startTime = ParseManager.parseString(json, objectKey: "start_time")
        _endTime = ParseManager.parseString(json, objectKey: "end_time")
        _roomNumber = ParseManager.parseString(json, objectKey: "room_number")
        _groupName = ParseManager.parseString(json, objectKey: "group_name")
        _groupId = ParseManager.parseInt(json, objectKey: "group_id")
        _teacherPhotoId = ParseManager.parseString(json, objectKey: "teacher_photo_id")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        aDecoder.encodeObject(_title, forKey: "title")
        aDecoder.encodeInteger(_day.rawValue, forKey: "day")
        aDecoder.encodeObject(_teacherName, forKey: "teacherName")
        aDecoder.encodeObject(_type, forKey: "type")
        aDecoder.encodeObject(_periodicity, forKey: "periodicity")
        aDecoder.encodeObject(_startTime, forKey: "startTime")
        aDecoder.encodeObject(_endTime, forKey: "endTime")
        aDecoder.encodeObject(_roomNumber, forKey: "roomNumber")
        aDecoder.encodeObject(_groupName, forKey: "groupName")
        aDecoder.encodeInteger(_groupId, forKey: "groupId")
        aDecoder.encodeObject(_teacherPhotoId, forKey: "teacherPhotoId")
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        _title = aCoder.decodeObjectForKey("title") as! String
        _day = WeekDay(rawValue: aCoder.decodeIntegerForKey("day"))
        _teacherName = aCoder.decodeObjectForKey("teacherName") as! String
        _type = aCoder.decodeObjectForKey("type") as! String
        _periodicity = aCoder.decodeObjectForKey("periodicity") as! String
        _startTime = aCoder.decodeObjectForKey("startTime") as! String
        _endTime = aCoder.decodeObjectForKey("endTime") as! String
        _roomNumber = aCoder.decodeObjectForKey("roomNumber") as! String
        _groupName = aCoder.decodeObjectForKey("groupName") as! String
        _groupId = aCoder.decodeIntegerForKey("groupId")
        _teacherPhotoId = aCoder.decodeObjectForKey("teacherPhotoId") as! String
    }
}
