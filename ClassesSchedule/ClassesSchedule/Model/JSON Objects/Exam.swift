//
//  Exam.swift
//  ClassesSchedule
//
//  Created by Ayham on 25/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class Exam: NSObject, NSCoding {

    //MARK:
    var title: String {
        return _title
    }
    
    var dateTime: String {
        return _dateTime
    }
    
    var roomNumber: String {
        return _roomNumber
    }
    
    var teacherName: String {
        return _teacherName
    }
    
    var id: Int {
        return _id
    }
    
    var groupId: Int {
        return _groupId
    }
    
    var groupName: String {
        return _groupName
    }
    
    //MARK: Printable
    override var description: String {
        
        return "id: \(id) title: \(title)  Date and time: \(_dateTime) Teacher name: \(_teacherName)        Room number: \(_roomNumber) Group id: \(_groupId) Group name: \(_groupName)"
    }
    
    override var debugDescription: String {
        return description
    }
    
    //MARK: Private properties
    private var _title: String = ""
    private var _dateTime: String = ""
    private var _roomNumber: String = ""
    private var _teacherName: String = ""
    private var _id: Int = 0
    private var _groupId: Int = 0
    private var _groupName: String = ""
    
    //MARK: Inits
    init(JSON json: JSON){
        
        _id = ParseManager.parseInt(json, objectKey: "id")
        _title = ParseManager.parseString(json, objectKey: "title")
        _dateTime = ParseManager.parseString(json, objectKey: "date_time")
        _teacherName = ParseManager.parseString(json, objectKey: "teacher_name")
        _roomNumber = ParseManager.parseString(json, objectKey: "room_number")
        _groupId = ParseManager.parseInt(json, objectKey: "group_id")
        _groupName = ParseManager.parseString(json, objectKey: "group_name")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        aDecoder.encodeInteger(_id, forKey: "id")
        aDecoder.encodeObject(_title, forKey: "title")
        aDecoder.encodeObject(_dateTime, forKey: "dateTime")
        aDecoder.encodeObject(_teacherName, forKey: "teacherName")
        aDecoder.encodeObject(_roomNumber, forKey: "roomNumber")
        aDecoder.encodeInteger(_groupId, forKey: "groupId")
        aDecoder.encodeObject(_groupName, forKey: "groupName")
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        _id = aCoder.decodeIntegerForKey("id")
        _title = aCoder.decodeObjectForKey("title") as! String
        _dateTime = aCoder.decodeObjectForKey("dateTime") as! String
        _teacherName = aCoder.decodeObjectForKey("teacherName") as! String
        _roomNumber = aCoder.decodeObjectForKey("roomNumber") as! String
        _groupName = aCoder.decodeObjectForKey("groupName") as! String
        _groupId = aCoder.decodeIntegerForKey("groupId")
    }
}
