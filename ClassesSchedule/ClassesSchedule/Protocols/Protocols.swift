//
//  Protocols.swift
//  Taxik Client
//
//  Created by Ayham on 15/10/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

protocol Singletonable {
    
    static var sharedManager: Self { get }
}

protocol Countable {
    
    var count: Int { get }
}
