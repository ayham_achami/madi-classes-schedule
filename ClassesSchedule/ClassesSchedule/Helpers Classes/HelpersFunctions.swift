//
//  HelpersFunctions.swift
//  Taxik Client
//
//  Created by Ayham on 14/07/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

func currentSystemVersionEquelOrBiggerThan(version: String) -> Bool {
    
    if UIDevice.currentDevice().systemVersion ==  version {
        
        return true
    }
    else{
        
        let systemVersion = UIDevice.currentDevice().systemVersion
        let deviceSystemVersion = NSDecimalNumber(string: systemVersion)
        let iOSVersion = NSDecimalNumber(string: version)
        
        if deviceSystemVersion.doubleValue >= iOSVersion.doubleValue {
            
            return true
        }
        else{
            return false
        }
    }
}

func IsRightToLeftUserInterfaces() -> Bool {
    
    return UIApplication.sharedApplication().userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.RightToLeft
}

func uuid() -> String {
    
    let uuidRef: CFUUIDRef = CFUUIDCreate(nil)
    let uuidStringRef: CFStringRef = CFUUIDCreateString(nil, uuidRef)
    let uuidNSString : NSString = uuidStringRef
    return uuidNSString as String
}

//MARK:  App Colors
func colorWithRGB(red: Int, green: Int, blue: Int, alpha: CGFloat) -> UIColor {
    
    let r: CGFloat = CGFloat(red) / 255.0
    let b: CGFloat = CGFloat(blue) / 255.0
    let g: CGFloat = CGFloat(green) / 225.0
    
    return UIColor(red: r, green: b, blue: g, alpha: alpha)
}

func CSRedColor(alpha: CGFloat = 1.0) -> UIColor {
    
    return UIColor(red: 1.0, green: 0.415, blue: 0.415, alpha: alpha)
}

func CSYellowColor(alpha: CGFloat = 1.0) -> UIColor {
    
    //184 134 11
    //return UIColor(red: 1.0, green: 0.756, blue: 0.145, alpha: alpha)
    return UIColor(red: 0.721, green: 0.525, blue: 0.043, alpha: alpha)
}

func CSGryeColor(alpha: CGFloat = 1.0) -> UIColor {
    
    return UIColor(white: 0.2, alpha: alpha)
}

func CSGryeColorForAlertButtom(alpha: CGFloat = 1.0) -> UIColor {
    
    return UIColor(white: 0.4, alpha: alpha)
}

func CSWhiteColor() -> UIColor {
    
    return UIColor(white: 1.0, alpha: 0.94)
}

func CSBlueColor() -> UIColor {
    
    return colorWithRGB(30, green: 144, blue: 255, alpha: 1.0)
}

func CSGreenColor(alpha: CGFloat = 1.0) -> UIColor {
    
    return colorWithRGB(126, green: 242, blue: 195, alpha: alpha)
}

func CSColorForProgressView() -> UIColor {
    
    return UIColor(white: 0.2, alpha: 0.8)
}
