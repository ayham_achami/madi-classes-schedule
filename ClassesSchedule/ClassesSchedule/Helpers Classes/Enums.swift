//
//  Enums.swift
//  Taxik Client
//
//  Created by Ayham on 27/09/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

//MARK: Http request method
enum HttpRequestMethod : String {
    
    case Get = "GET", Post = "POST"
}

//MARK: Taxik clinet API requests
enum MADIAPIReques: String, CustomStringConvertible, CustomDebugStringConvertible {

    
//    /*1*/  case GetGroups = "GetGroups.php"
//    /*2*/  case GetTeachersNames = "GetTeachersNames.php"
//    /*3*/  case GetExamByGroup = "GetExamByGroup.php?group_id="
//    /*4*/  case GetSchedulebyGroup = "GetSchedulebyGroup.php?group_id="
//    /*5*/  case GetSchedulebyTeacher = "GetSchedulebyTeacher.php?techer_id="
    
    //Requestes
    /*1*/  case GetGroups = "GetGroups.php"
    /*2*/  case GetTeachersNames = "GetTeachersNames.php"
    /*3*/  case GetExamByGroup = "GetExamByGroup.php"
    /*4*/  case GetSchedulebyGroup = "GetSchedulebyGroup.php"
    /*5*/  case GetSchedulebyTeacher = "GetSchedulebyTeacher.php"
    
    var description: String {
        
        return rawValue
    }
    
    var debugDescription: String {
        
        return description
    }
}

//MARK: Device display
enum DeviceDisplay: Int {
    
    case Unknown = 0, iPad, iPhone35Inch, iPhone4Inch, iPhone47Inch, iPhone55Inch
}

//MARK: Error types
enum ErrorType: Int, CustomStringConvertible, CustomDebugStringConvertible {
    
    case MustIgnore = 0, Internal, Server, Network
    
    var description: String {
        
        switch self {
            case .MustIgnore:
                return "MustIgnore"
            case .Internal:
                return "Internal"
            case .Server:
                return "Server"
            case .Network:
                return "Network"
        }
    }
    
    var debugDescription: String {
        return description
    }
}

//MARK: Server errors
enum ServerError: Int {
    
    case JSONDecodingError = 0
    case BDRequestError = 1
    case DateError = 2
    case InternalServerError = 500//Internal server error
}

enum WeekDay: Int, CustomStringConvertible, CustomDebugStringConvertible {
    
    case None = 0
    case Monday = 1
    case Tuesday = 2
    case Wednesday = 3
    case Thursday = 4
    case Friday = 5
    case Saturday = 6
    case Sunday = 7
    
    init(rawValue: Int) {
        
        switch rawValue {
            
            case 0:
                self = .None
            case 1:
                self = .Monday
            case 2:
                self = .Tuesday
            case 3:
                self = .Wednesday
            case 4:
                self = .Thursday
            case 5:
                self = .Friday
            case 6:
                self = .Saturday
            case 7:
                self = .Sunday
            default:
                self = .None
        }
    }
    
    var description: String {
        
        switch self {
        case .None:
            return NSLocalizedString("None", comment: "None")
        case .Monday:
            return NSLocalizedString("Monday", comment: "Monday")
        case .Tuesday:
            return NSLocalizedString("Tuesday", comment: "Tuesday")
        case .Wednesday:
            return NSLocalizedString("Wednesday", comment: "Wednesday")
        case .Thursday:
            return NSLocalizedString("Thursday", comment: "Thursday")
        case .Friday:
            return NSLocalizedString("Friday", comment: "Friday")
        case .Saturday:
            return NSLocalizedString("Saturday", comment: "Saturday")
        case .Sunday:
            return NSLocalizedString("Sunday", comment: "Sunday")
        }
    }
    
    var debugDescription: String {
        return description
    }
}