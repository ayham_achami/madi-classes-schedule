//
//  AppConfiguration.swift
//  Taxik Client
//
//  Created by Ayham on 27/09/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

//iOS version const's
let iOS8 = "8.0"
let iOS7_1 = "7.1"
let iOS7 = "7.0"
let iOS6 = "6.0"
let iOS5 = "5.0"
let iOS4 = "4.0"

let clocks = ["🕛","🕐","🕑","🕒","🕓","🕔","🕕","🕖","🕗","🕘","🕙","🕚"]

//NotificationsKeys
let NC_REACHABILITY_STATUS_CHANGED_NOTIFICATION = "ncReachabilityStatusChanged"

//User info notification keys
let UIDK_REACHABILITY_STATUS = "TCReachabilityStatus"

//User defaults keys
let defaultUserGroupKey = "defaultUserGroupKey"

//internal error codes
let emptyResponseErrorCode = -1
let canceledNetworkRequestsErrorCode = -2
let responseIsUndicationarObjectErorrCode = -3
let unableToParseREsponseErrorCode = -4

struct AppConfiguration {
    
    //Hint: Don't forget to change the runtime mode when build the release build
    ///when true the log not be work, and the app has connected to production server
    private static let isProduction = true
    ///when true the log will be work, and the app has connected to production server
    private static let isProductionTest = false
    
    static var isDebugMode : Bool {
        get{
            if isProduction || isProductionTest {
                return false
            }
            else{
                return true
            }
        }
    }
    
    static var LogEnable : Bool {
        get{
            if isProduction {
                return false
            }
            else{
                return true
            }
        }
    }
    
    static var isRunOnSimulator : Bool {
        get{
            #if TARGET_IPHONE_SIMULATOR
                return true
            #else
                return false
            #endif
        }
    }
}
