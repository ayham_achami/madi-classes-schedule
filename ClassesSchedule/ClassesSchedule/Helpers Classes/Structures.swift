//
//  Structures.swift
//  Taxik Client
//
//  Created by Ayham on 27/09/14.
//  Copyright (c) 2014 Taxik. All rights reserved.
//

import Foundation

//MARK: Logger
struct Log {
    
    static func debug(funcationName: String = __FUNCTION__, linaNumber: Int = __LINE__, info: String){
        
        if AppConfiguration.LogEnable {
            NSLog("Debug info from method: [\(funcationName)] line number: [\(linaNumber)]{\n\(info)\n}\n")
        }
    }
    
    static func error(faileName: String = __FILE__, linaNumber: Int = __LINE__, funcationName: String = __FUNCTION__, error: String){
        
        NSLog("Erorr in file: [\(faileName)] line number: [\(linaNumber)] funcation name: [\(funcationName)]{\n\(error)\n}\n")
    }
}

//MARK: API error
struct APIError: CustomStringConvertible, CustomDebugStringConvertible {
    
    var error: NSError
    var type: ErrorType
    
    init(JSON json:JSON, domin aDomin: NSURL){
        
        type = ErrorType.Server
        error = ParseManager.parseServerError(JSON: json, domain: aDomin)
    }
    
    init(error aError: NSError, type aType: ErrorType){
        
        type = aType
        error = aError
    }
    
    var description: String {
        return error.localizedDescription
    }
    
    var debugDescription: String {
        return "Domain: \(error.domain) Code: \(error.code) userInfo: \(error.userInfo) Error typе: \(type.description)"
    }
}
