//
//  SplashSegue.swift
//  Taxik Client
//
//  Created by Ayham on 17/03/15.
//  Copyright (c) 2015 Taxik. All rights reserved.
//

import UIKit

class SplashSegue: UIStoryboardSegue {
    
    override func perform() {
        
        let source = sourceViewController.view as UIView!
        _ = destinationViewController.view
        let keyWindow = UIApplication.sharedApplication().keyWindow!
        
        keyWindow.backgroundColor = colorWithRGB(247, green: 247, blue: 247, alpha: 1)
        
        UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10.0, options: .CurveEaseInOut, animations: { () -> Void in
            
            source.transform = CGAffineTransformMakeScale(0.90, 0.90)
            
        }) { (finished) -> Void in
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                
                source.transform = CGAffineTransformMakeScale(10.0, 10.0)
                source.alpha = 0.0
                
            }, completion: { (isFinished) -> Void in
              
                keyWindow.rootViewController = self.destinationViewController
            })
        }
    }
}
