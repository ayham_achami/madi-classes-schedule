//
//  AlphaChangeSegue.swift
//  Taxik Client
//
//  Created by Ayham on 18/03/15.
//  Copyright (c) 2015 Taxik. All rights reserved.
//

import UIKit

class AlphaChangeSegue: UIStoryboardSegue {
    
    override func perform() {
        
        let source = sourceViewController.view as UIView!
        let keyWindow = UIApplication.sharedApplication().keyWindow!
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            
            source.alpha = 0.0
            
            }, completion: { (isFinished) -> Void in
                
                keyWindow.rootViewController = self.destinationViewController
        })
    }
}
