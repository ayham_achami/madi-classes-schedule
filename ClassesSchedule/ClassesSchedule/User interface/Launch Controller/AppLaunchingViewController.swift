//
//  AppLaunchingViewController.swift
//  Taxik Client
//
//  Created by Ayham on 17/03/15.
//  Copyright (c) 2015 Taxik. All rights reserved.
//

import UIKit

class AppLaunchingViewController: UIViewController, GroupsListViewControllerDelegate {
    
    //MARK: IBOutlet properties
    @IBOutlet weak var loadBaseView: UIView!
    @IBOutlet weak var loader: DGActivityIndicatorView!
    
    //MARK: Private properties
    private var groupsList: [Group] = []
    private var overlayView: MRProgressOverlayView!
    private let launchingTimeOut: Double = 3.0
    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        loader.type = DGActivityIndicatorAnimationType.NineDots
        loader.tintColor = view.tintColor
        loader.size = 50
        
        //NSLocalizedString("Loading", comment: "Loading")
        loadGroupsListIfNeeded()
    }
    
    //MARK: Private funcation
    private func loadGroupsListIfNeeded() {
        
        loader.startAnimating()
        if !ArchivManager.hasChoiceGroupBefore() {
            
            //Load groups list and display it
            MADIAPIManager.sharedManager.showProgressOverlayView = false
            RequestManager.loadGroupsList { (groups: [Group]?, error: APIError?) -> Void in
                
                if let groupsList = groups {
                    
                    self.loader.stopAnimating()
                    self.groupsList = groupsList
                    self.performSegueWithIdentifier("ShowGroupsList", sender: self)
                    MADIAPIManager.sharedManager.showProgressOverlayView = true
                }
                else{
                    
                    ErrorHandlerManager.handleError(error!, autoRatry: true, completion: {self.loadGroupsListIfNeeded()})
                }
            }
        }
        else{
            
            //show my schedule vc
            let dispatchTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(launchingTimeOut * Double(NSEC_PER_SEC)))
            dispatch_after(dispatchTime, dispatch_get_main_queue()) { () -> Void in
                
                self.loader.stopAnimating()
                self.performSegueWithIdentifier("ShowRootVC", sender: self)
            }
        }
    }
    
    //MARK: Groups list view controller delegate
    func groupsList(grroupsList: GroupsListViewController, didEndSelectedGroup group: Group) {
        
        ArchivManager.sharedManager.userGroup = group
        grroupsList.dismissViewControllerAnimated(true, completion: { () -> Void in
            
            self.performSegueWithIdentifier("ShowRootVC", sender: self)
        })
    }

    //MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowGroupsList" {
            
            let groupsListVC = (segue.destinationViewController as! UINavigationController).visibleViewController as! GroupsListViewController
            groupsListVC.groupsList = groupsList
            groupsListVC.delegate = self
        }
    }
}
