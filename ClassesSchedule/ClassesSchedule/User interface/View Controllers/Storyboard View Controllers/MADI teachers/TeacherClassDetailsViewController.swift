//
//  TeacherClassDetailsViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 11/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class TeacherClassDetailsViewController: UIViewController {
    
    //MARK: IBOutlet properties
    @IBOutlet weak var titleLable1: UILabel!
    @IBOutlet weak var titleLable2: UILabel!
    @IBOutlet weak var titleLable3: UILabel!
    @IBOutlet weak var titleLable4: UILabel!
    @IBOutlet weak var titleLable5: UILabel!
    @IBOutlet weak var titleLable6: UILabel!
    @IBOutlet weak var classTitleLabel: UILabel!
    @IBOutlet weak var classTimeLabel: UILabel!
    @IBOutlet weak var classTypeLabel: UILabel!
    @IBOutlet weak var classPeriodicityLabel: UILabel!
    @IBOutlet weak var classRoomLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    
    //MARK: Public properties
    var teacherCalss: TeacherClass?

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if IsRightToLeftUserInterfaces(){
            
            titleLable1.textAlignment = .Right
            titleLable2.textAlignment = .Right
            titleLable3.textAlignment = .Right
            titleLable4.textAlignment = .Right
            titleLable5.textAlignment = .Right
            titleLable6.textAlignment = .Right
            classTypeLabel.textAlignment = .Right
            classPeriodicityLabel.textAlignment = .Right
            classRoomLabel.textAlignment = .Right
            groupNameLabel.textAlignment = .Right
        }

        classTitleLabel.text = teacherCalss?.title
        classTimeLabel.text = teacherCalss?.classTimeInterval
        classTypeLabel.text = teacherCalss?.type
        classPeriodicityLabel.text = teacherCalss?.periodicity
        classRoomLabel.text = teacherCalss?.roomNumber
        groupNameLabel.text = teacherCalss?.groupName
    }
}
