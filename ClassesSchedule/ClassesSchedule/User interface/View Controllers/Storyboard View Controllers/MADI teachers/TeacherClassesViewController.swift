//
//  TeacherClassesViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 11/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class TeacherClassesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: IBOutlet properties
    @IBOutlet weak var teacherImageView: UIImageView!
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var tabelView: UITableView!
    
    //MARK: Public properties
    var teacher: Teacher?
    
    //MARK: Private properties
    private var teacherClassesReference: [WeekDay: [TeacherClass]] = [:]
    private var teacherClasses: [WeekDay: [TeacherClass]] = [:]
    private var keys: [WeekDay] = []

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if let teach = teacher {
            
            teacherNameLabel.textAlignment = IsRightToLeftUserInterfaces() ? NSTextAlignment.Right : NSTextAlignment.Left
            teacherNameLabel.text = teach.name
            
            teacherImageView.layer.masksToBounds = true
            teacherImageView.layer.cornerRadius = teacherImageView.frame.height / 2
            teacherImageView.layer.borderColor = self.view.tintColor.CGColor
            teacherImageView.layer.borderWidth = 3.0
            if let photoURL = RequestManager.urlForTeacherPhotoWithId(teach.photo) {
                
                let placeholderImage = UIImage(named: "nopic.jpg")
                teacherImageView.setImageWithURL(photoURL, placeholderImage: placeholderImage)
            }
            else{
                
                teacherImageView.image = UIImage(named: "nopic.jpg")
            }
            
            loadTeacherCalsses(teach)
        }
    }
    
    //MARK:
    @IBAction func valueChanged(sender: AnyObject) {
        //TODO: change classes type
    }
    
    // MARK: Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        //return teacherClasses.count
        let keys = Array(teacherClasses.keys)
        return keys.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let day = keys[section]
        if let classes = teacherClasses[day] {
            
            return classes.count
        }
        else{
            
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TeacherClassesScheduleCell", forIndexPath: indexPath) 
        
        let day = keys[indexPath.section]
        if let classes = teacherClasses[day] {
            
            cell.textLabel?.text = classes[indexPath.row].title
            cell.detailTextLabel?.text = classes[indexPath.row].classTimeInterval
        }
        
        return cell
    }
    
    //MARK: Table view delegate
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let day = keys[section]
        return day.description
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    //MARK: Private functions
    private func loadTeacherCalsses(teacher: Teacher) {
        
        RequestManager.loadClassesScheduleForTeacher(teacher, compilation: { (teacherClasses, error) -> Void in
            
            if let classes = teacherClasses {
                
                self.teacherClasses = classes
                self.teacherClassesReference = classes
                self.keys = Array(self.teacherClassesReference.keys)
                self.keys.sortInPlace{ $0.rawValue < $1.rawValue}
                self.tabelView.reloadData()
            }
            else{
                
                ErrorHandlerManager.handleError(error!, autoRatry: false, completion: {self.loadTeacherCalsses(teacher)})
            }
        })
    }
    
    // MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showTeacherClassDetails" {
            
            let teacherClassDetailsVC = segue.destinationViewController as! TeacherClassDetailsViewController
            if let indexPath = self.tabelView.indexPathForSelectedRow {
                
                let day = keys[indexPath.section]
                if let classes = teacherClasses[day] {
                    
                    teacherClassDetailsVC.teacherCalss = classes[indexPath.row]
                }
            }
            
        }
    }
}
