//
//  TeacherListTableViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 11/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class TeacherListTableViewController: UITableViewController, UISearchResultsUpdating {

    //MARK: Private properties
    private var teachersList: [Teacher] = []
    private var filteredTeachersList: [Teacher] = []
    private var selectesTeacher: Teacher?
    private var searchController = UISearchController()
    
    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        loadTeachersList()
    }
    
    //MARK: Actions
    @IBAction func showMenu(sender: AnyObject) {
        
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    //MARK: Search support
    func updateSearchResultsForSearchController(searchController: UISearchController) {
     
        filteredTeachersList.removeAll(keepCapacity: false)
        let searchText = searchController.searchBar.text!
        filteredTeachersList = teachersList.filter({ (group) -> Bool in
            
            let tmp: NSString = group.name
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        
        self.tableView.reloadData()
    }

    //MARK: Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.active {
            
            return filteredTeachersList.count
        }
        else{
            
            return teachersList.count
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("teacherCellIdentifier", forIndexPath: indexPath) 

        // Configure the cell...
        var teacher: Teacher
        if searchController.active {
            
            teacher = filteredTeachersList[indexPath.row]
        }
        else{
            
            teacher = teachersList[indexPath.row]
        }
        
        let teachePhotoImageView = cell.viewWithTag(100) as! UIImageView
        teachePhotoImageView.contentMode = UIViewContentMode.ScaleAspectFill
        teachePhotoImageView.layer.masksToBounds = true
        teachePhotoImageView.layer.cornerRadius = teachePhotoImageView.frame.height / 2
        teachePhotoImageView.layer.borderColor = self.view.tintColor.CGColor
        teachePhotoImageView.layer.borderWidth = 3.0
        if let photoURL = RequestManager.urlForTeacherPhotoWithId(teacher.photo) {
            
            let placeholderImage = UIImage(named: "nopic.jpg")
            teachePhotoImageView.setImageWithURL(photoURL, placeholderImage: placeholderImage)
        }
        else{
            
            teachePhotoImageView.image = UIImage(named: "nopic.jpg")
        }

        let label = cell.viewWithTag(101) as! UILabel
        label.textAlignment = IsRightToLeftUserInterfaces() ? NSTextAlignment.Right : NSTextAlignment.Left
        label.text = teacher.name
        
        return cell
    }
    
    //MARK: Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if searchController.active {
            
            self.selectesTeacher = filteredTeachersList[indexPath.row]
        }
        else{
            
            self.selectesTeacher = teachersList[indexPath.row]
        }
        
        searchController.active = false
        performSegueWithIdentifier("showTeacherClasses", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    //MARK: Private functions
    private func loadTeachersList(){
        
        RequestManager.loadTeachersList { (MADITeachers, error) -> Void in
            
            if let teachers = MADITeachers {
                
                self.teachersList = teachers
                self.tableView.reloadData()
            }
            else{
                
                ErrorHandlerManager.handleError(error!, autoRatry: false, completion: {self.loadTeachersList()})
            }
        }
    }

    // MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showTeacherClasses" {
            
            let teacherClassesVC = segue.destinationViewController as! TeacherClassesViewController
            teacherClassesVC.teacher = self.selectesTeacher
        }
    }
}
