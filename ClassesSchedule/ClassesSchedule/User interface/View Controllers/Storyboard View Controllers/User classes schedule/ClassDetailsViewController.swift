//
//  ClassDetailsViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 09/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class ClassDetailsViewController: UIViewController {

    //MARK: IBOutlet properties
    @IBOutlet weak var titleLable1: UILabel!
    @IBOutlet weak var titleLable2: UILabel!
    @IBOutlet weak var titleLable3: UILabel!
    @IBOutlet weak var titleLable4: UILabel!
    @IBOutlet weak var titleLable5: UILabel!
    @IBOutlet weak var titleLable6: UILabel!
    @IBOutlet weak var classTitleLabel: UILabel!
    @IBOutlet weak var classTimeLabel: UILabel!
    @IBOutlet weak var classTypeLabel: UILabel!
    @IBOutlet weak var classPeriodicityLabel: UILabel!
    @IBOutlet weak var classRoomLabel: UILabel!
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var teachePhotoImageView: UIImageView!
    
    //MARK: Public properties
    var userClass: SemesterClass?
    
    //MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if IsRightToLeftUserInterfaces() {
            
            titleLable1.textAlignment = .Right
            titleLable2.textAlignment = .Right
            titleLable3.textAlignment = .Right
            titleLable4.textAlignment = .Right
            titleLable5.textAlignment = .Right
            titleLable6.textAlignment = .Right
            classTypeLabel.textAlignment = .Right
            classPeriodicityLabel.textAlignment = .Right
            classRoomLabel.textAlignment = .Right
        }
        
        classTitleLabel.text = userClass?.title
        classTimeLabel.text = userClass?.classTimeInterval
        classTypeLabel.text = userClass?.classType
        classPeriodicityLabel.text = userClass?.preiodicity
        classRoomLabel.text = userClass?.room
        teacherNameLabel.text = userClass?.teacherName
        
        teachePhotoImageView.contentMode = UIViewContentMode.ScaleAspectFill
        teachePhotoImageView.layer.masksToBounds = true
        teachePhotoImageView.layer.cornerRadius = 50.0
        teachePhotoImageView.layer.borderColor = self.view.tintColor.CGColor
        teachePhotoImageView.layer.borderWidth = 3.0
        teachePhotoImageView.layer.rasterizationScale = UIScreen.mainScreen().scale;
        teachePhotoImageView.layer.shouldRasterize = true;
        teachePhotoImageView.clipsToBounds = true
        
        if let photoId = userClass?.techerPhotoId {
            
            if let photoURL = RequestManager.urlForTeacherPhotoWithId(photoId) {
            
                let placeholderImage = UIImage(named: "nopic.jpg")
                teachePhotoImageView.setImageWithURL(photoURL, placeholderImage: placeholderImage)
            }
            else{
                
                teachePhotoImageView.image = UIImage(named: "nopic.jpg")
            }
        }
        else{
            
            teachePhotoImageView.image = UIImage(named: "nopic.jpg")
        }
    }
}
