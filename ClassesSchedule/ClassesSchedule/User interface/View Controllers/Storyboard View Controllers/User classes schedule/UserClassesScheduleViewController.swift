//
//  UserClassesScheduleTableViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 07/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit
import CoreData

class UserClassesScheduleViewController: UIViewController {
    
    //MARK: IBOutlet properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hintLabel: UILabel!

    //MARK: Public properties
    var userClasses: [UserClass] = []
    var frc: NSFetchedResultsController?
    
    //MARK: View life cycle
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        loadClasses()
    }
    
    //MARK: Actions
    @IBAction func showMenu(sender: AnyObject) {
        
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @IBAction func segmentControlValueChanged(sender: UISegmentedControl) {
        
        if let group = ArchivManager.sharedManager.userGroup {
            
            switch sender.selectedSegmentIndex {
            
                case 0:
                    self.frc = CoreDataMeneger.sharedManager.getTodayCalsses(group)
                    displayInfoLabelIfNeeded()
                case 1:
                    self.frc = CoreDataMeneger.sharedManager.getWeekCalsses(group)
                    displayInfoLabelIfNeeded()
                case 2:
                    self.frc = CoreDataMeneger.sharedManager.getUserClass(group)
                    displayInfoLabelIfNeeded()
                default:
                    self.frc = CoreDataMeneger.sharedManager.getUserClass(group)
                    displayInfoLabelIfNeeded()
            }
        }
    }

    //MARK: Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if let sectionCount = frc?.sections?.count {
        
            return sectionCount
        }
        else{
            
            return 1
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let userClasses = frc?.sections {
            
            let classes = userClasses[section]
            return classes.numberOfObjects
        }
        else{
            
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("UserClassesScheduleCell", forIndexPath: indexPath) 

        
        if let classs = frc?.objectAtIndexPath(indexPath) as? SemesterClass {
            
            cell.textLabel?.text = classs.title
            cell.detailTextLabel?.text = classs.classTimeInterval
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if let sections = frc?.sections {
            
            let currentSection = sections[section] 
            if let dayNumber = Int(currentSection.name) {
                
                let day = WeekDay(rawValue: dayNumber)
                return day.description
            }
            else{
                
                return nil
            }
        }
        
        return nil
    }
    
    //MARK: Private functions
    private func loadClasses(){
        
        if frc == nil {
            
            if let group = ArchivManager.sharedManager.userGroup {
                
                RequestManager.loadClassesScheduleForGroup(group, compilation: { (group, classes, error) -> Void in
                    
                    if let notOptionalClasses = classes {
                        
                        self.userClasses = notOptionalClasses
                        CoreDataMeneger.sharedManager.saveUserClasses(self.userClasses)
                        self.frc = CoreDataMeneger.sharedManager.getTodayCalsses(group)
                        self.displayInfoLabelIfNeeded()
                    }
                    else if let _ = error {
                        
                        ErrorHandlerManager.handleError(error!, autoRatry: false, completion: {self.loadClasses()})
                        self.frc = CoreDataMeneger.sharedManager.getTodayCalsses(group)
                        self.displayInfoLabelIfNeeded()
                    }
                    else{
                        
                        //never be came here
                        self.frc = CoreDataMeneger.sharedManager.getTodayCalsses(group)
                        self.displayInfoLabelIfNeeded()
                    }
                })
            }
        }
    }
    
    private func displayInfoLabelIfNeeded() {
        
        if frc?.sections?.count != 0 {
            
            //hide info label
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
                
                self.hintLabel.alpha = 0
                self.tableView.alpha = 1
                
            }, completion: { (finished) -> Void in
                
                if finished {
                    
                    self.hintLabel.hidden = true
                    self.tableView.hidden = false
                    self.tableView.reloadData()
                }
            })
        }
        else{
            //show info label
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
                
                self.tableView.alpha = 0
                self.hintLabel.alpha = 1
                
                }, completion: { (finished) -> Void in
                    
                    if finished {
                        
                        self.hintLabel.hidden = false
                        self.tableView.hidden = true
                    }
            })
        }
    }
    
    //MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showClassDetails" {
            
            if let indexPath = tableView.indexPathForSelectedRow {
                
                let classDetailsVC = segue.destinationViewController as! ClassDetailsViewController
                if let classs = frc?.objectAtIndexPath(indexPath) as? SemesterClass {
                    
                    classDetailsVC.userClass = classs
                }
            }
        }
    }
}
