//
//  TaxikNavigationController.swift
//  Taxik Client
//
//  Created by Ayham on 17/03/15.
//  Copyright (c) 2015 Taxik. All rights reserved.
//

import UIKit

class AppNavigationController: UINavigationController, UIGestureRecognizerDelegate {

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: Selector("panGestureRecognized:"))
        gestureRecognizer.delegate = self
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    //MARK: Pan gesture recognizer
    func panGestureRecognized(sender: UIPanGestureRecognizer) {
        
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    //MARK: Pan gesture recognizer delegate
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if self.viewControllers.count > 1 {
            
            return false
        }
        else{
            
            return true
        }
    }
}
