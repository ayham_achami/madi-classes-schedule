//
//  GroupsListTableViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 05/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

protocol GroupsListViewControllerDelegate {
    
    func groupsList(grroupsList: GroupsListViewController, didEndSelectedGroup group: Group) -> Void
}

class GroupsListViewController: UIViewController, UISearchResultsUpdating {
    
    //MARK: IBOutlet properties
    @IBOutlet var tableView: UITableView!
    
    //MARK: Public properties
    var groupsList: [Group] = []
    var delegate: GroupsListViewControllerDelegate?
    
    //MARK: private @IBOutlet
    private var searchController = UISearchController()
    private var filteredGroupsList: [Group] = []

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.tintColor = self.view.tintColor
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    //MARK: Search support
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        filteredGroupsList.removeAll(keepCapacity: false)
        let searchText = searchController.searchBar.text!
        filteredGroupsList = groupsList.filter({ (group) -> Bool in
            
            let tmp: NSString = group.name
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        
        self.tableView.reloadData()
    }
    
    // MARK: Table view data source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.active {
            
            return filteredGroupsList.count
        }
        else{
            
            return groupsList.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("groupCellIdentifier", forIndexPath: indexPath) 

        var group: Group
        if searchController.active {
            
            group = filteredGroupsList[indexPath.row]
        }
        else{
            
            group = groupsList[indexPath.row]
        }
        
        cell.textLabel?.text = group.name
        return cell
    }
    
    //MARK: Table view delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var group: Group
        if searchController.active {
            
            group = filteredGroupsList[indexPath.row]
        }
        else{
            
            group = groupsList[indexPath.row]
        }
        tableView .deselectRowAtIndexPath(indexPath, animated: true)

        searchController.active = false
        delegate?.groupsList(self, didEndSelectedGroup: group)
    }
}
