//
//  ExamDetailsViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 25/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class ExamDetailsViewController: UIViewController {
    
    //MARK: @IBOutlet properties
    @IBOutlet weak var titleLable1: UILabel!
    @IBOutlet weak var titleLable2: UILabel!
    @IBOutlet weak var titleLable3: UILabel!
    @IBOutlet weak var titleLable4: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeDateLabel: UILabel!
    @IBOutlet weak var roomNumberLabel: UILabel!
    @IBOutlet weak var teacherNameLabel: UILabel!
        
    //MARK: Public properties
    var exam: Exam?

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if IsRightToLeftUserInterfaces() {
            
            titleLable1.textAlignment = .Right
            titleLable2.textAlignment = .Right
            titleLable3.textAlignment = .Right
            titleLable4.textAlignment = .Right
            roomNumberLabel.textAlignment = .Right
            teacherNameLabel.textAlignment = .Right
        }
        
        titleLabel.text = exam?.title
        timeDateLabel.text = exam?.dateTime
        roomNumberLabel.text = exam?.roomNumber
        teacherNameLabel.text = exam?.teacherName
    }
}
