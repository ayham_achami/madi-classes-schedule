//
//  UserExamsViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 25/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class UserExamsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: IBOutlet properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hintLabel: UILabel!
    
    //MARK: Privaet properties
    private var exams: [Exam] = []

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadExams()
    }
    
    //MARK: Actions
    @IBAction func showMenu(sender: AnyObject) {
        
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    //MARK: Table view data source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return exams.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("UserExamCell", forIndexPath: indexPath) 
        
        let exam = exams[indexPath.row]
        cell.textLabel?.text = exam.title
        cell.detailTextLabel?.text = exam.dateTime
        
        return cell
    }
    
    //MARK: Private function
    private func loadExams() {
        
        if let group = ArchivManager.sharedManager.userGroup {
            
            RequestManager.loadExamsForGroup(group) { (group, exams, error) -> Void in
                
                if let examsList = exams {
                    
                    self.exams = examsList
                    self.displayInfoLabelIfNeeded(nil)
                }
                else{
                    
                    let message = ErrorHandlerManager.handleError(error!, autoRatry: false, completion: {self.loadExams()})
                    self.displayInfoLabelIfNeeded(message)
                }
            }
        }
    }
    
    private func displayInfoLabelIfNeeded(message: String?) {
        
        if exams.count != 0 {
            
            //hide info label
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
                
                self.hintLabel.alpha = 0
                self.tableView.alpha = 1
                
                }, completion: { (finished) -> Void in
                    
                    if finished {
                        
                        self.hintLabel.hidden = true
                        self.tableView.hidden = false
                        self.tableView.reloadData()
                    }
            })
        }
        else{
            //show info label
            if let msg = message {
                
                hintLabel.text = msg
            }
            
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
                
                self.tableView.alpha = 0
                self.hintLabel.alpha = 1
                
                }, completion: { (finished) -> Void in
                    
                    if finished {
                        
                        self.hintLabel.hidden = false
                        self.tableView.hidden = true
                    }
            })
        }
    }
    
    // MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showExamDetails" {
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                
                let examDetailsVC = segue.destinationViewController as! ExamDetailsViewController
                examDetailsVC.exam = exams[indexPath.row]
            }
        }
    }
}
