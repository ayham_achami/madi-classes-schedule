//
//  SettingsTableViewController.swift
//  ClassesSchedule
//
//  Created by Ayham on 11/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, GroupsListViewControllerDelegate {

    //MARK: Private properties
    private var groupsList: [Group] = []
    
    //MARK: View life cycle
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        if let currentLanguage = appCurrentLanguage() {
            
            switch currentLanguage {
                
                case "en":
                    changeAppLanguage(currentLanguage, index: 0)
                case "ar":
                    changeAppLanguage(currentLanguage, index: 1)
                case "ru":
                    changeAppLanguage(currentLanguage, index: 2)
                default:
                    break
            }
        }
    }
    
    //MARK: Actions
    @IBAction func showMenu(sender: AnyObject) {
        
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    //MARK: Private functions
    private func appCurrentLanguage() -> String? {
        
        if let languages = NSUserDefaults.standardUserDefaults().objectForKey("AppleLanguages") as? [String] {
         
            return languages.first!
        }
        else{
            
            return nil
        }
    }
    
    private func changeAppLanguage(language: String, index: Int){
        
        let languages = [language]
        NSUserDefaults.standardUserDefaults().setObject(languages, forKey: "AppleLanguages")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        switch index {
            
            case 0:
                let indexPath1 = NSIndexPath(forItem: 0, inSection: 1)
                let indexPath2 = NSIndexPath(forItem: 1, inSection: 1)
                let indexPath3 = NSIndexPath(forItem: 2, inSection: 1)
                let cell1 = tableView.cellForRowAtIndexPath(indexPath1)
                let cell2 = tableView.cellForRowAtIndexPath(indexPath2)
                let cell3 = tableView.cellForRowAtIndexPath(indexPath3)
                cell1?.accessoryType = .Checkmark
                cell2?.accessoryType = .None
                cell3?.accessoryType = .None
            case 1:
                let indexPath1 = NSIndexPath(forItem: 0, inSection: 1)
                let indexPath2 = NSIndexPath(forItem: 1, inSection: 1)
                let indexPath3 = NSIndexPath(forItem: 2, inSection: 1)
                let cell1 = tableView.cellForRowAtIndexPath(indexPath1)
                let cell2 = tableView.cellForRowAtIndexPath(indexPath2)
                let cell3 = tableView.cellForRowAtIndexPath(indexPath3)
                cell1?.accessoryType = .None
                cell2?.accessoryType = .Checkmark
                cell3?.accessoryType = .None
            case 2:
                let indexPath1 = NSIndexPath(forItem: 0, inSection: 1)
                let indexPath2 = NSIndexPath(forItem: 1, inSection: 1)
                let indexPath3 = NSIndexPath(forItem: 2, inSection: 1)
                let cell1 = tableView.cellForRowAtIndexPath(indexPath1)
                let cell2 = tableView.cellForRowAtIndexPath(indexPath2)
                let cell3 = tableView.cellForRowAtIndexPath(indexPath3)
                cell1?.accessoryType = .None
                cell2?.accessoryType = .None
                cell3?.accessoryType = .Checkmark
            default:
                break
        }
    }
    
    private func loadGroupList() {
        
        RequestManager.loadGroupsList({ (groups, error) -> Void in
            
            if let list = groups {
                
                self.groupsList = list
                self.performSegueWithIdentifier("ShowChangeMyGroupVC", sender: self)
            }
            else{
                
                ErrorHandlerManager.handleError(error!, autoRatry: false, completion: {self.loadGroupList()})
            }
        })
    }
    
    //MARK: Groups list view controller delegate
    func groupsList(grroupsList: GroupsListViewController, didEndSelectedGroup group: Group) {
        
        grroupsList.navigationController?.popViewControllerAnimated(false)
        let format = NSLocalizedString("Change_user_group_alert_message", comment: "message")
        let message =  String.localizedStringWithFormat(format, ArchivManager.sharedManager.userGroup!.name ,group.name)
        let title = NSLocalizedString("Change_user_group_alert_title", comment: "title")
        AlertManager.showAlertOnView(self, title: title, message: message, firstButtonAction: { () -> Void in
            
            CoreDataMeneger.sharedManager.deleteSemesterClasses()
            ArchivManager.sharedManager.userGroup = group
            
        }) { () -> Void in
            
            //group change canceled
        }
    }

    //MARK: Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                
                if groupsList.count == 0 {
                
                    loadGroupList()
                }
                else{
                
                    self.performSegueWithIdentifier("ShowChangeMyGroupVC", sender: self)
                }
            }
        }
        else if indexPath.section == 1 {
            
            switch indexPath.row {
                
                case 0:
                    changeAppLanguage("en", index: 0)
                case 1:
                    changeAppLanguage("ar", index: 1)
                case 2:
                    changeAppLanguage("ru", index: 2)
                default:
                    break
            }
        }
    }
    
    //MARK: Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowChangeMyGroupVC" {
            
            let groupsListVC = segue.destinationViewController as! GroupsListViewController
            groupsListVC.groupsList = self.groupsList
            groupsListVC.delegate = self
        }
    }
}
