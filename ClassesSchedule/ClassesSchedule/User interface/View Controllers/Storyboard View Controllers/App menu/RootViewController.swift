//
//  RootViewController.swift
//  Taxik Client
//
//  Created by Ayham on 18/03/15.
//  Copyright (c) 2015 Taxik. All rights reserved.
//

import UIKit

class RootViewController: REFrostedViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationDidChangeStatusBarOrientationNotification:", name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
    }

    //MARK: Nib load
    override func awakeFromNib() {
        
        self.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("UserCalssesNavController")
        self.menuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("menuViewController")
        
        let display = UIDevice.currentDevice().display
        if display == .iPad ||
           display == .iPhone55Inch ||
           display == .iPhone47Inch {
            
            var newSize = UIScreen.mainScreen().bounds.size
            newSize.width = 320
            self.menuViewSize = newSize
        }
        else {
            
            var newSize = UIScreen.mainScreen().bounds.size
            newSize.width = 250
            self.menuViewSize = newSize
        }
    }
    
    @objc func applicationDidChangeStatusBarOrientationNotification(notify: NSNotification) {
        
        let display = UIDevice.currentDevice().display
        if display == .iPad ||
           display == .iPhone55Inch ||
           display == .iPhone47Inch {
            
            var newSize = UIScreen.mainScreen().bounds.size
            newSize.width = 320
            self.menuViewSize = newSize
        }
        else {
            
            var newSize = UIScreen.mainScreen().bounds.size
            newSize.width = 250
            self.menuViewSize = newSize
        }
    }
}
