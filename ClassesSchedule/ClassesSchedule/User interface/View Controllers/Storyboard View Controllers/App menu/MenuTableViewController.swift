//
//  MenuTableViewController.swift
//  Taxik Client
//
//  Created by Ayham on 18/03/15.
//  Copyright (c) 2015 Taxik. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    //MARK: Private properties
    private var groupNameLabel: UILabel!
    private let menuItemsCount = 4
    private let menuItemNames = [NSLocalizedString("My Calsses", comment: "My Calsses"),
                                 NSLocalizedString("My Exams", comment: "My Exams"),
                                 NSLocalizedString("Teachers list", comment: "Teachers list"),
                                 NSLocalizedString("Settings", comment: "Settings")]
    private let menuIcons = ["myclasses", "myexams", "teacherslist", "settings"]

    //MARK: View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let headerViewClosure = {() -> UIView in
        
            let view = UIView(frame: CGRectMake(0, 0, 0, 184.0))
            let imageView = UIImageView(frame: CGRectMake(0, 40, 100, 100))
        
            imageView.autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin];
            imageView.contentMode = UIViewContentMode.Center
            imageView.image = UIImage(named: "student.png")
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 50.0
            imageView.layer.borderColor = self.view.tintColor.CGColor
            imageView.layer.borderWidth = 3.0
            imageView.layer.rasterizationScale = UIScreen.mainScreen().scale;
            imageView.layer.shouldRasterize = true;
            imageView.clipsToBounds = true
        
            self.groupNameLabel = UILabel(frame: CGRectMake(0, 150, 0, 24))
            if let group = ArchivManager.sharedManager.userGroup {
                
                self.groupNameLabel.text = group.name
            }
            else{
                
                self.groupNameLabel.text = NSLocalizedString("MADI", comment: "MADI")
            }
            self.groupNameLabel.backgroundColor = UIColor.clearColor()
            self.groupNameLabel.textColor = self.view.tintColor
            self.groupNameLabel.sizeToFit()
            self.groupNameLabel.autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin];

            view.addSubview(imageView)
            view.addSubview(self.groupNameLabel)
            return view
        }
        
        self.tableView.tableHeaderView = headerViewClosure()
        self.tableView.separatorColor = self.view.tintColor
        self.tableView.backgroundColor = UIColor.clearColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        if let group = ArchivManager.sharedManager.userGroup {
            
            groupNameLabel.text = group.name
            groupNameLabel.sizeToFit()
        }
    }

    // MARK: Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuItemsCount
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("menuCellIdentifier", forIndexPath: indexPath) 
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        let label = cell.viewWithTag(2) as! UILabel
        
        label.text = menuItemNames[indexPath.row]
        label.textAlignment = IsRightToLeftUserInterfaces() ? NSTextAlignment.Right : NSTextAlignment.Left
        imageView.image = UIImage(named: menuIcons[indexPath.row])
        return cell
    }
    
    //MARK: Table view delegate
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.backgroundColor = UIColor.clearColor()
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        switch indexPath.row {
            case 0:
                self.frostedViewController.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("UserCalssesNavController")
            case 1:
                self.frostedViewController.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("UserExamsNavController")
                break
            case 2:
                self.frostedViewController.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TeacherListNavController")
            case 3:
                //SettingsVC
                self.frostedViewController.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsNavController") 
                break
            default:
                break
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.frostedViewController.hideMenuViewController()
    }
}
