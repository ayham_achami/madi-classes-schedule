//
//  AppDelegate.swift
//  ClassesSchedule
//
//  Created by Ayham on 05/04/15.
//  Copyright (c) 2015 Ayham Hylam. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        MADIAPIManager.sharedManager.configureManagerWithBaseURL()
        
        //Hint: Uncomment this code to clear app data
//        ArchivManager.sharedManager.userGroup = nil
//        CoreDataMeneger.sharedManager.clearAppData()
//        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: defaultUserGroupKey)
//        NSUserDefaults.standardUserDefaults().synchronize()
        
        return true
    }
}

